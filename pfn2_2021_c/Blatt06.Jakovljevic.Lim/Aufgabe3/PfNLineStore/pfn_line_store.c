#include "pfn_line_store.h"
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <assert.h>

struct PfNLineStore
{
    size_t nextfree; /* number of lines */
    PfNLine *lines; /* array with references to lines */
    char separator;
};

PfNLineStore *pfn_line_store_new(unsigned char *file_contents, \
size_t size, char sep)
{
    unsigned int count;
    unsigned int i;
    count = 0;
    for (i = 0; i < size; i++)
    {
        if (file_contents[i] == sep){
            count++;
        }
    }

    struct PfNLineStore * result;

    result = malloc(sizeof(PfNLineStore));

    if (result == NULL) {
        fprintf(stderr, "malloc(%lu)  failed\n", sizeof(PfNLineStore));
        exit(EXIT_FAILURE);
    }

    PfNLine * lines;
    

    lines = malloc(sizeof(lines) * count + 1);

    if (lines == NULL) {
        fprintf(stderr, "malloc(%lu)  failed\n", sizeof(lines) * count + 1);
        free(result);
        exit(EXIT_FAILURE);
    }
    
    result->nextfree = count;
    result->lines = lines;
    result->separator = sep;

    unsigned char * nextline;
    nextline = file_contents;
    
    count = 0;
    for (i = 0; i < size; i++)
    {
        if (file_contents[i] == sep){
            file_contents[i] = 0;
            lines[count++] = (PfNLine) nextline;
            nextline = file_contents + i + 1;
        }
    }

    lines[count] = (PfNLine) nextline;

    return result;

}

void pfn_line_store_delete(PfNLineStore *pfn_line_store)
{
    if (pfn_line_store != NULL)
    {
        if (pfn_line_store->lines != NULL)
        {
            free(pfn_line_store->lines);
        }
        free(pfn_line_store);
    }
}

size_t pfn_line_store_number(const PfNLineStore *pfn_line_store)
{
    assert(pfn_line_store != 0);
    return pfn_line_store->nextfree;
}

PfNLine pfn_line_store_access(const PfNLineStore *pfn_line_store,size_t idx)
{
    assert(pfn_line_store != 0);
    return pfn_line_store->lines[idx];
}

char pfn_line_store_sep(const PfNLineStore *pfn_line_store)
{
    assert(pfn_line_store != 0);
    return pfn_line_store->separator;
}

void pfn_line_store_show(const PfNLineStore *pfn_line_store)
{
    assert(pfn_line_store != 0);
    int i;
    for (i = 0; i < pfn_line_store->nextfree; i++)
    {
        printf("%s\n", pfn_line_store->lines[i]);
    }
    
}

