#ifndef FS_PRIO_STORE_H
#define FS_PRIO_STORE_H
#include <stdbool.h>
#include <stddef.h>

#define UNUSED __attribute__ ((unused))

/* We store pairs of a double valued key (the experimental data, such
   as a measurement) and a corresponding index, which may be interpreted
   as a time point or a representation of a location at which the
   measurement was created. We denote the measurement value by key, because
   the ordering of the items is based on this key. */

typedef struct
{
  double key; /* can be experimental measurement */
  unsigned long index;  /* identifier, stands for location or time point of
                           measurement */
} KeyIndexPair;

/* As usual, in the header file we only provide the name of the
   structure by introducing a type name. So we have an opaque type.
   A declaration of the form
   struct FSPrioStore
   {
      ....
   };
   must appear in the C-file fs_prio_store.c, which must include this header
   file. */

typedef struct FSPrioStore FSPrioStore;

/* return a pointer to a dynamically allocated store for a fixed number of
   elements, which is specified by the parameter capacity. The
   capacity parameter (must be > 0). */

FSPrioStore *fs_prio_store_new(size_t capacity);

/* delete the store */

void fs_prio_store_delete(FSPrioStore *fsps);

/* return the number of elements stored */

size_t fs_prio_store_size(const FSPrioStore *fsps);

/* return true iff the store is full, that is, it contains capacity
   many elements, where capacity was specified in the _new method. */

bool fs_prio_store_is_full(const FSPrioStore *fsps);

/* When applying this function, we assume that the elements which
   have been added before are stored in an array in ascending order according
   to the key. The function adds the element referred to by kip, if the store
   is empty
   or kip->key is smaller than the key of at least one of the elements already
   stored. In case the store is already full, an existing element will be
   overwritten. After possibly adding a new element, all stored elements
   are in ascending order according to the key.
   As the order of the
   elements is maintained, elements often have to be moved to
   different locations in the array. The number of moves is the return value. */

size_t fs_prio_store_add_smallest(FSPrioStore *fsps,const KeyIndexPair *kip);

/* Return the element at index idx in the store. idx must be smaller
   than sz = fs_prio_store_size(fsps). If the elements were added
   by fs_prio_store_add_smallest, then the pair with smallest key
   is a index 0 and the one with the largest key is at index sz - 1.
   If the elements were added
   by fs_prio_store_add_largest, then the pair with largest key
   is a index 0 and the one with the smallest key is at index sz - 1.
 */

KeyIndexPair fs_prio_store_at(const FSPrioStore *fsps,size_t idx);

/* It is not necessary to fully implement the following method. Its call
   appears in the main program, but in the tests the function is never called.
   So it suffices to add the following to the C-file:

size_t fs_prio_store_add_largest(UNUSED FSPrioStore *fsps,
                                 UNUSED const KeyIndexPair *kip)
{
  return 0;
}
*/


size_t fs_prio_store_add_largest(FSPrioStore *fsps,const KeyIndexPair *kip);


#endif
