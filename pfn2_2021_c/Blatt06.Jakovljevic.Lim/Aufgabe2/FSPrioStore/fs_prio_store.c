#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include "fs_prio_store.h"

struct FSPrioStore {
    KeyIndexPair *data;
    size_t capacity;
    size_t elements;
};


FSPrioStore *fs_prio_store_new(size_t capacity) {
    assert(capacity != 0);

    FSPrioStore *fsps = malloc(sizeof *fsps);
    assert(fsps != NULL);

    fsps->data = malloc((capacity+1) * sizeof *fsps->data);
    assert(fsps->data != NULL);

    fsps -> elements = 0;
    fsps -> capacity = capacity;

    return fsps;
}


void fs_prio_store_delete(FSPrioStore *fsps) {
    if (fsps != NULL) {
        if (fsps->data != NULL)
            free(fsps->data);

        free(fsps);
    }
}


size_t fs_prio_store_size(const FSPrioStore *fsps) {
    assert(fsps != NULL);
    return fsps -> elements;
}


bool fs_prio_store_is_full(const FSPrioStore *fsps) {
    assert(fsps != NULL);
    return (fsps -> elements == fsps -> capacity) ? true : false;
}


size_t fs_prio_store_add_smallest(FSPrioStore *fsps,const KeyIndexPair *kip) {
    size_t moves = 0;

    assert(fsps != NULL);
    assert(fsps->data != NULL);

    if (fsps -> elements == 0) {
        fsps -> data[0] = *kip;
        (fsps -> elements)++;
    }
    else {
        for (size_t i = fsps->elements; i>0; i--) {
            if (fsps->data[i-1].key <= kip->key) {
                fsps->data[i] = *kip;

                if (!fs_prio_store_is_full(fsps))
                    (fsps->elements)++;
                return moves;
            }
            else {
                fsps->data[i] = fsps->data[i-1];
                moves++;

                if (i==1) {
                    fsps->data[0] = *kip;
                    if (!fs_prio_store_is_full(fsps))
                        (fsps->elements)++;
                }
            }
        }
    }
    return moves;
}


KeyIndexPair fs_prio_store_at(const FSPrioStore *fsps,size_t idx) {
    assert(fsps != NULL);
    assert(fsps->data != NULL);
    assert(idx < fsps->elements);

    return fsps->data[idx];
}


size_t fs_prio_store_add_largest(UNUSED FSPrioStore *fsps,
                                 UNUSED const KeyIndexPair *kip)
{
    return 0;
}
