#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "fs_prio_store.h"

static void output_extreme_values(const FSPrioStore *fsps,
                                  const char *tag,
                                  bool increasing)
{
  printf("# %s elements\t%lu\n",tag,fs_prio_store_size(fsps));
  const size_t num_elements = fs_prio_store_size(fsps);
  for (size_t idx = 0; idx < num_elements; idx++)
  {
    const size_t access_idx = increasing ? idx : num_elements - 1 - idx;
    KeyIndexPair kip = fs_prio_store_at(fsps,access_idx);
    printf("%lu\t%.8lf\t%lu\n",access_idx,kip.key,kip.index);
  }
}

typedef enum
{
  Undefined, /* must be first */
  OnlySmallest,
  OnlyLargest,
  SmallestLargest,
} Processmode;

static Processmode arg2process_mode(const char *tag)
{
  const char *tags[] = {"s","l","sl"};
  for (size_t idx = 0; idx < sizeof tags/sizeof tags[0]; idx++)
  {
    if (strcmp(tag,tags[idx]) == 0)
    {
      return (Processmode) (idx+1);
    }
  }
  return Undefined;
}

static FSPrioStore *fill_fsp_store(const KeyIndexPair *present,
                                   size_t num_present,
                                   const KeyIndexPair *new_elem)
{
  FSPrioStore *fsp = fs_prio_store_new(3);
  for (size_t idx = 0; idx < num_present; idx++)
  {
    fs_prio_store_add_smallest(fsp,present + idx);
  }
  fs_prio_store_add_smallest(fsp,new_elem);
  return fsp;
}

static bool key_index_pair_equal(KeyIndexPair a,KeyIndexPair b)
{
  return a.key == b.key && a.index == b.index;
}

static void unit_test_1(void)
{
  KeyIndexPair data[] = {{4.0,0},{7.0,1}},
               new_elem = {5.0,2};
  const size_t num_elems = sizeof data/sizeof data[0];
  FSPrioStore *fsp = fill_fsp_store(data,num_elems,&new_elem);
  assert(fs_prio_store_size(fsp) == 3);
  assert(key_index_pair_equal(fs_prio_store_at(fsp,0),data[0]));
  assert(key_index_pair_equal(fs_prio_store_at(fsp,1),new_elem));
  assert(key_index_pair_equal(fs_prio_store_at(fsp,2),data[1]));
  fs_prio_store_delete(fsp);
}

static void unit_test_2(void)
{
  KeyIndexPair data[] = {{4.0,0},{7.0,1}},
               new_elem = {1.0,2};
  const size_t num_elems = sizeof data/sizeof data[0];
  FSPrioStore *fsp = fill_fsp_store(data,num_elems,&new_elem);
  assert(fs_prio_store_size(fsp) == 3);
  assert(key_index_pair_equal(fs_prio_store_at(fsp,0),new_elem));
  assert(key_index_pair_equal(fs_prio_store_at(fsp,1),data[0]));
  assert(key_index_pair_equal(fs_prio_store_at(fsp,2),data[1]));
  fs_prio_store_delete(fsp);
}

static void unit_test_3(void)
{
  KeyIndexPair data[] = {{4.0,0},{7.0,1}},
               new_elem = {8.0,2};
  const size_t num_elems = sizeof data/sizeof data[0];
  FSPrioStore *fsp = fill_fsp_store(data,num_elems,&new_elem);
  assert(fs_prio_store_size(fsp) == 3);
  assert(key_index_pair_equal(fs_prio_store_at(fsp,0),data[0]));
  assert(key_index_pair_equal(fs_prio_store_at(fsp,1),data[1]));
  assert(key_index_pair_equal(fs_prio_store_at(fsp,2),new_elem));
  fs_prio_store_delete(fsp);
}

static void unit_test_4(void)
{
  KeyIndexPair data[] = {{4.0,0},{7.0,1},{8.0,2}},
               new_elem = {9.0,3};
  const size_t num_elems = sizeof data/sizeof data[0];
  FSPrioStore *fsp = fill_fsp_store(data,num_elems,&new_elem);
  assert(fs_prio_store_size(fsp) == 3);
  for (size_t idx = 0; idx < sizeof data/sizeof data[0]; idx++)
  {
    assert(key_index_pair_equal(fs_prio_store_at(fsp,idx),data[idx]));
  }
  fs_prio_store_delete(fsp);
}

static void unit_test_5(void)
{
  KeyIndexPair data[] = {{4.0,0},{7.0,1},{8.0,2}},
               new_elem = {5.0,3};
  const size_t num_elems = sizeof data/sizeof data[0];
  FSPrioStore *fsp = fill_fsp_store(data,num_elems,&new_elem);
  assert(fs_prio_store_size(fsp) == 3);
  assert(key_index_pair_equal(fs_prio_store_at(fsp,0),data[0]));
  assert(key_index_pair_equal(fs_prio_store_at(fsp,1),new_elem));
  assert(key_index_pair_equal(fs_prio_store_at(fsp,2),data[1]));
  fs_prio_store_delete(fsp);
}

static void unit_test_6(void)
{
  KeyIndexPair data[] = {{4.0,0},{7.0,1},{8.0,2}},
               new_elem = {3.0,3};
  const size_t num_elems = sizeof data/sizeof data[0];
  FSPrioStore *fsp = fill_fsp_store(data,num_elems,&new_elem);
  assert(fs_prio_store_size(fsp) == 3);
  assert(key_index_pair_equal(fs_prio_store_at(fsp,0),new_elem));
  assert(key_index_pair_equal(fs_prio_store_at(fsp,1),data[0]));
  assert(key_index_pair_equal(fs_prio_store_at(fsp,2),data[1]));
  fs_prio_store_delete(fsp);
}

static void unit_tests(void)
{
  unit_test_1();
  unit_test_2();
  unit_test_3();
  unit_test_4();
  unit_test_5();
  unit_test_6();
}

int main(int argc,char *argv[])
{
  int read_int;

  if (argc != 3 || arg2process_mode(argv[1]) == Undefined ||
      sscanf(argv[2],"%d",&read_int) != 1 || read_int < 1)
  {
    fprintf(stderr,"Usage: %s s|l|sl <capacity>\n",argv[0]);
    return EXIT_FAILURE;
  }
  unit_tests();
  const size_t capacity = (size_t) read_int;
  size_t moves[2] = {0};
  FSPrioStore *fsps[2];
  const Processmode process_mode = arg2process_mode(argv[1]);
  int indexes[3] = {-1,-1,-1};

  if (process_mode == OnlySmallest)
  {
    indexes[0] = 0;
  } else
  {
    if (process_mode == OnlyLargest)
    {
      indexes[0] = 1;
    } else
    {
      indexes[0] = 0;
      indexes[1] = 1;
    }
  }
  for (int j = 0; j < 3 && indexes[j] != -1; j++)
  {
    fsps[indexes[j]] = fs_prio_store_new(capacity);
  }
  double key;
  size_t input_items;
  for (input_items = 0; scanf("%lf\t%d",&key,&read_int) == 2; input_items++)
  {
    KeyIndexPair kip = {key,(unsigned int) read_int};
    for (int j = 0; j < 3 && indexes[j] != -1; j++)
    {
      int i = indexes[j];
      moves[i] += ((i == 0) ? fs_prio_store_add_smallest
                            : fs_prio_store_add_largest)
                              (fsps[i],&kip);
    }
  }
  printf("# number of input items\t%lu\n",input_items);
  for (int j = 0; j < 3 && indexes[j] != -1; j++)
  {
    int i = indexes[j];
    output_extreme_values(fsps[i],i == 0 ? "smallest" : "largest", i == 0);
    printf("# number of moves per input item\t%.2f\n",
            (double) moves[i]/input_items);
    fs_prio_store_delete(fsps[i]);
  }
  return EXIT_SUCCESS;
}
