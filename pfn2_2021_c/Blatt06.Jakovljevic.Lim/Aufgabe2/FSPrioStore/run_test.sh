#!/bin/bash

if test $# -ne 3
then
  echo "Usage: $0 s|l|sl <number of elements> <number of extremes to select>"
  exit 1
fi

mode=$1
number=$2
extremes=$3

INPUTFILE=`mktemp /tmp/kvp.XXXXXX` || exit 1
OUTPUTFILE=`mktemp /tmp/extremes.XXXXXX` || exit 1
smallest=0
largest=0
if test ${mode} = "s"
then
  smallest=1
else
  if test ${mode} = "l"
  then
    largest=1
  else
    if test ${mode} == "sl"
    then
      smallest=1
      largest=1
    fi
  fi
fi
./experiment.py ${number} > ${INPUTFILE}
touch ${OUTPUTFILE}
if test $smallest -eq 1
then
  sort -n ${INPUTFILE} | sort -n | head -n ${extremes} > ${OUTPUTFILE}
fi
if test $largest -eq 1
then
  sort -n ${INPUTFILE} | sort -n | tail -n ${extremes} >> ${OUTPUTFILE}
fi
cat ${INPUTFILE} | ./fs_prio_store.x ${mode} ${extremes} | grep -v '^#' | cut -f 2,3 | diff - ${OUTPUTFILE}
rm -f ${INPUTFILE} ${OUTPUTFILE}
