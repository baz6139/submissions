#ifndef RANDSEQPROB_H
#define RANDSEQPROB_H
#include <stddef.h>

char *random_sequence_prob(const char *alphabet,const double *prob,size_t len);

#endif
