#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <limits.h>
#include <assert.h>
#include "randseqprob.h"

static double sum_up(const double *table,size_t num_elems)
{
  double sum = 0.0;
  for (size_t idx = 0; idx < num_elems; idx++)
  {
    sum += table[idx];
  }
  return sum;
}

static void test_random_sequence_prob(const char *progname,
                                      const char *alphabet,
                                      const double *prob,
                                      size_t num_probs,
                                      size_t len)
{
  double tolerance = len < 1000000 ? 1e-2 : 1e-3;
  uint8_t symbolmap[UCHAR_MAX+1];
  const uint8_t undefined = UCHAR_MAX;
  size_t alpha_size = strlen(alphabet);
  assert(alpha_size == num_probs);
  size_t *count = calloc(alpha_size,sizeof *count);
  printf("# alphabet\t%s\n",alphabet);
  printf("# total prob\t%.4f\n",sum_up(prob,alpha_size));
  printf("# tolerance\t%.1e\n",tolerance);
  uint8_t rank = 0;
  for (size_t idx = 0; idx <= UCHAR_MAX; idx++)
  {
    symbolmap[idx] = undefined;
  }
  for (size_t idx = 0; idx < alpha_size; idx++)
  {
    symbolmap[(int) alphabet[idx]] = rank++;
  }
  char *rseq = random_sequence_prob(alphabet,prob,len);
  for (char *rptr = rseq; rptr < rseq + len; rptr++)
  {
    const uint8_t rank = symbolmap[(int) *rptr];
    if (rank == undefined)
    {
      fprintf(stderr,"%s: character %c is not a valid character in "
                     "alphabet %s\n",progname,*rptr,alphabet);
      exit(EXIT_FAILURE);
    }
    count[rank]++;
  }
  printf("# expected prob\tevaluated prob\n");
  for (size_t idx = 0; idx < alpha_size; idx++)
  {
    const double expected_prob = prob[idx];
    const double evaluated_prob = (double) count[idx]/len;
    printf("%.4f\t%.4f\n",expected_prob,evaluated_prob);
    if (fabs(expected_prob - evaluated_prob) > tolerance)
    {
      fprintf(stderr,"%s: expected probability %.4f != %.4f "
                     "evaluated probability\n",
                     progname,expected_prob,evaluated_prob);
      exit(EXIT_FAILURE);
    }
  }
  free(rseq);
  free(count);
}

#define TABLE_SIZE(TAB) (sizeof (TAB)/sizeof (TAB)[0])

int main(int argc,char *argv[])
{
  const char *bases = "ACGT";
  double increasing_prob[] = {0.1,0.2,0.3,0.4};
  double skewed_prob[] = {0.01,0.01,0.01,0.97};
  double vac_prob[] = {0.3334,0.1669,0.1671,0.3326};
  const char *aminos = "ACDEFGHIKLMNPQRSTVWY";
  const double aa_prob[] =
  {
    0.08259,
    0.01382,
    0.05462,
    0.06735,
    0.03865,
    0.07079,
    0.02276,
    0.05925,
    0.05817,
    0.09656,
    0.02415,
    0.04060,
    0.04732,
    0.03933,
    0.05536,
    0.06626,
    0.05359,
    0.06866,
    0.01098,
    0.02920
  };
  long len;

  if (argc != 2 || sscanf(argv[1],"%ld",&len) != 1 || len < 1)
  {
    fprintf(stderr,"Usage: %s <len>\n",argv[0]);
    return EXIT_FAILURE;
  }
  test_random_sequence_prob(argv[0],bases,increasing_prob,
                            TABLE_SIZE(increasing_prob),(size_t) len);
  test_random_sequence_prob(argv[0],bases,skewed_prob,
                            TABLE_SIZE(skewed_prob),(size_t) len);
  test_random_sequence_prob(argv[0],bases,vac_prob,
                            TABLE_SIZE(vac_prob),(size_t) len);
  test_random_sequence_prob(argv[0],aminos,aa_prob,
                            TABLE_SIZE(aa_prob),(size_t) len);
  return EXIT_SUCCESS;
}
