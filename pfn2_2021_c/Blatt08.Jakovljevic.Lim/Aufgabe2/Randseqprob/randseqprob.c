Aufgabe 4

SPLITARRAY(arr)
    if length(arr) <= 1
        return arr
    else
        return [arr[:length(array)/2], arr[length(arr)/2]]

MERGETEILARRAYS_REC(arr1, b, arr2, m, zielarr, c, i, j)
    for arr1[b] <- i to j && arr2[m] <- i to j
        if b < length(arr1) && m < length(arr2)
            if arr1[b] < arr2[m]
                zielarr[c] <- arr1[b]
                b++
            else
                zielarr[c] <- arr2[m]
                m++
            c++
            MERGEARRAY_REC(arr1, b, arr2, m, zielarr, c, i, j)
        else
            while b < length(arr1)
                zielarr[c] <- arr1[b]
                b++
                c++
            while m < length(arr2)
                zielarr[c] <- arr2[m]
                m++
                c++
    return zielarr





#include "randseqprob.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


char *random_sequence_prob(const char *alphabet,const double *prob,size_t n)
{
    size_t alpha_len = strlen(alphabet);
    
    char *seq = malloc(n * sizeof(*seq));
    assert(seq != NULL);
    double *total_prob = malloc(alpha_len * sizeof(*total_prob));
    assert(total_prob != NULL);
    double sum = 0.0;
 
    for (size_t i = 0; i < alpha_len; i++)
    {
        total_prob[i] = sum;
        sum += prob[i];
    }

    for (size_t i = 0; i< n; i++)
    {
        double random = drand48();
        size_t left = 0, right = alpha_len - 1;
        while (left <= right)
        {
            size_t mid = (left + right) / 2;
            
            if ((total_prob[mid] == total_prob[alpha_len-1]) || \
                (random >= total_prob[mid] && random < total_prob[mid + 1]))
            {
                seq[i] = alphabet[mid];
                break;
            }
           
            if (random < total_prob[mid]) right = mid - 1;
            else left = mid + 1;
        }
        
    }
    return seq;
}


SPLITARRAY(arr)
    if length(arr) <= 1
        return arr
    else
        return [arr[:length(array)/2], arr[length(arr)/2]]

MERGETEILARRAYS_REC(arr1, b, arr2, m, zielarr, c, i, j)
    for arr1[b] <- i to j && arr2[m] <- i to j
        if b < length(arr1) && m < length(arr2)
            if arr1[b] < arr2[m]
                zielarr[c] <- arr1[b]
                b++
            else
                zielarr[c] <- arr2[m]
                m++
            c++
            MERGEARRAY_REC(arr1, b, arr2, m, zielarr, c, i, j)
        else
            while b < length(arr1)
                zielarr[c] <- arr1[b]
                b++
                c++
            while m < length(arr2)
                zielarr[c] <- arr2[m]
                m++
                c++
    return zielarr
