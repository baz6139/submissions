#!/bin/sh

if test $# -eq 1
then
  inputkind=$1
  option=
else
  if test $# -eq 2
  then
    inputkind=$2
    option=$1
  else
    echo "Usage: $0 [-n] <inputkind>"
    exit 1
  fi
fi

TMPFILE=`mktemp /tmp/TMP.XXXXXX` || exit 1

for reversed in "" "-r"
do
  ./sort_simple.x ${option} ${reversed} ${inputkind}.txt > ${TMPFILE}
  if test $? -ne 0
  then
    echo "$0: ./sort_simple.x ${option} ${reversed} ${inputkind}.txt FAILED"
    exit 1
  fi
  diff ${TMPFILE} ${inputkind}_sorted${option}${reversed}.txt
  if test $? -ne 0
  then
    echo "$0: output of"
    echo "./sort_simple.x ${option} ${reversed} ${inputkind}.txt"
    echo "differs from exptected output in"
    echo "${inputkind}_sorted${option}${reversed}.txt"
    exit 1
  fi
done
