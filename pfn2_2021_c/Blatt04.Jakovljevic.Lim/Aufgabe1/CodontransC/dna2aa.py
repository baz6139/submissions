#!/usr/bin/env python
import sys, re, argparse
from codon2aa import codon2aa
from reverse_complement import reverse_complement
from print_sequence import print_sequence
from fastaIterator import fasta_next

def cds2protein(seq):
  aa_list = list()
  for codon in re.findall(r'[ACGT]{3}',seq,flags=re.I):
    aa = codon2aa(codon)
    aa_list.append(aa)
  return ''.join(aa_list)

def parse_arguments():
  p = argparse.ArgumentParser(description=('transform coding sequence into '
                                           'protein'))
  p.add_argument('-w','--width',type=int,default=70,
                 help='specify width of line in fasta formatted output')
  p.add_argument('inputfile',type=str,
                  help='specify input file with coding sequence')
  return p.parse_args()

def msg(length,inputfile,plength,forward):
  sys.stderr.write(('{}: translate DNA sequence of length {} from file {} '
                    'to protein sequence of length {}, forward={}\n')
                    .format(sys.argv[0],length,inputfile,plength,forward))

args = parse_arguments()

try:
  stream = open(args.inputfile)
except IOError as err:
  sys.stderr.write('{}: {}\n'.format(sys.argv[0],err))
  exit(1)

for _, sequence in fasta_next(args.inputfile):
  protein = cds2protein(sequence)
  msg(len(sequence),args.inputfile,len(protein),'true')
  print('>translation of sequence from {}'.format(args.inputfile))
  print_sequence(protein,args.width)
  sequence = reverse_complement(sequence)
  protein = cds2protein(sequence)
  msg(len(sequence),args.inputfile,len(protein),'false')
  print('>reverse complement translation of sequence from {}'
          .format(args.inputfile))
  print_sequence(protein,args.width)
