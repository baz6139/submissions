#!/usr/bin/env python3
#title	Calculating the reverse complement of a DNA sequence
import string

#lst{ReverseFromReversed}
def reverse(seq):
  return ''.join(reversed(seq))
#lstend#

#lst{ReverseComplement}
def reverse_complement(seq):
  revcom = reverse(seq)
  transtab = str.maketrans('ACGTacgt','TGCAtgca')
  return revcom.translate(transtab)
#lstend#

if __name__ == '__main__':
  s = 'agctatcagcagcat'
  t = reverse_complement(s)
  assert s == reverse_complement(t), \
         's = {} != {} = RC({})\n'.format(s,reverse_complement(t),t)
