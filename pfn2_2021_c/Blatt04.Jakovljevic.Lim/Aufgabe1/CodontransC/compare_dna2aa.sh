#!/bin/sh

if test $# -ne 1
then
  echo "Usage: $0 <sequence length>"
  exit 1
fi

DNA_SEQUENCE_FILENAME=`mktemp /tmp/dna_seq.fnaXXXXXX` || exit 1
PROTEIN_SEQUENCE_FILENAME=`mktemp /tmp/protein_seq.fnaXXXXXX` || exit 1
length=$1
./random_sequence.py --dna --length ${length} > ${DNA_SEQUENCE_FILENAME}
time ./dna2aa.x ${DNA_SEQUENCE_FILENAME} > ${PROTEIN_SEQUENCE_FILENAME}
time ./dna2aa.py ${DNA_SEQUENCE_FILENAME} | diff - ${PROTEIN_SEQUENCE_FILENAME}
rm -f ${DNA_SEQUENCE_FILENAME} ${PROTEIN_SEQUENCE_FILENAME}
