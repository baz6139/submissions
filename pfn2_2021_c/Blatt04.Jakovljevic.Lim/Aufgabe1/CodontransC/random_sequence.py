#!/usr/bin/env python3

import argparse
from random import randint

def parse_arguments():
  p = argparse.ArgumentParser(description='generate random sequence')
  seqtypeclass = p.add_mutually_exclusive_group(required=True)
  seqtypeclass.add_argument('--dna',action='store_true',default=False,
                            help='generate DNA sequence')
  seqtypeclass.add_argument('--protein',action='store_true',default=False,
                            help='generate protein sequence')
  seqtypeclass.add_argument('--alphabet',type=str,default=None,
                            help='generate sequence over given alphabet')
  p.add_argument('-w','--width',type=int,default=70,
                 help='specify width of line in fasta formatted output')
  p.add_argument('-l','--length',type=int,default=1000,
                 help='specify length of sequence to generate')
  return p.parse_args()

args = parse_arguments()

alphabet = None
if args.dna:
  alphabet = 'ACGT'
elif args.protein:
  alphabet = 'ACDEFGHIKLMNPQRSTVWY'
else:
  alphabet = args.alphabet
print('>random sequence of length {} over alphabet {}'
       .format(args.length,alphabet))

line = list()
alpha_size = len(alphabet)
for idx in range(0,args.length):
  cc = alphabet[randint(0,alpha_size-1)]
  line.append(cc)
  if len(line) == args.width:
    print(''.join(line))
    line.clear()

if len(line) > 0:
  print(''.join(line))
