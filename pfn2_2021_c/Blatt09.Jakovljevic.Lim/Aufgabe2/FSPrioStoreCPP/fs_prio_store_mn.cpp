#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <vector>
#include "fs_prio_store.hpp"

class KeyIndexPair
{
  private:
    double key; /* can be experimental measurement */
    unsigned long index;  /* identifier, stands for location or time point of
                             measurement */
  public:
  
    // FRAGE 1: Wo wird dieser Konstruktor mit zwei Argumente ben"otigt?
    // Es wird f"ur Prozesse ben"otigt, die vom Benutzer initialisierte Werte
    // beinhalten, n"amlich die Methode 'add(...)' und operator[](...).
    
    KeyIndexPair(double _key,unsigned long _index)
    {
      key = _key;
      index = _index;
    }
    
    // FRAGE 2: Wo wird dieser Konstruktor ohne Argumente ben"otigt?
    // Es wird aufgerufen, wenn keine vom Benutzer initialisierte Werte
    // bereitgestellt werden und stellt sicher, dass Klassen mit vernünftigen
    // Standardwerten initialisiert werden. Das ist n"amlich wichtig f"ur
    // die Methode FSPriostore(...).
    
    KeyIndexPair(void)
    {
      key = 0.0;
      index = 0;
    }
    double key_get(void) const {return key;}
    unsigned long index_get(void) const {return index;}
    bool operator==(const KeyIndexPair &b) const
    {
      return this->key == b.key && this->index == b.index;
    }
    bool operator!=(const KeyIndexPair &b) const
    {
      return !(*this == b);
    }
    bool operator<(const KeyIndexPair &b) const
    {
      if (this->key < b.key) { return true; }
      if (this->key > b.key) { return false; }
      if (this->index < b.index) {return true;}
      if (this->index > b.index) {return false;}
      return false;
    }
    bool operator>(const KeyIndexPair &b) const
    {
      return !(*this < b) && *this != b;
    }
};

static bool smaller(const KeyIndexPair *a,const KeyIndexPair *b)
{
  // FRAGE 3: Welche Methode bzw. welcher Operator wird in der folgenden
  // return Anweisung aufgerufen?
  // Die Methode add(...) und der Operator <<.
  return *a < *b;
}

static bool larger(const KeyIndexPair *a,const KeyIndexPair *b)
{
  // FRAGE 4: Was dr"uckt das Zeichen * in der folgenden Zeile aus und warum
  // ist es notwendig?
  // Es druckt den hinzugef"ugten (bzw. zuvor vorhandenen) Wert und es ist
  // wichtig zu entscheiden, welche Methode von 'Add' wir aufrufen m"ussen.
  return *a > *b;
}

// FRAGE 5: Wozu dient dieser Operator <<?
// Es führt eine "Overloading" für alle von std::ostream abgeleiteten Klassen
// durch, um die KeyIndexPair-Klasse zu behandeln.
static std::ostream& operator<<(std::ostream& os, const KeyIndexPair &value)
{
  return os << std::fixed << std::setprecision(8) <<
         value.key_get() << "\t" << value.index_get();
}

static void output_extreme_values(const FSPrioStore<KeyIndexPair> *fsps,
                                  const char *tag,
                                  bool ascending)
{
  std::cout << "# " << tag << " elements\t" << fsps->size() << std::endl;
  const size_t numofelements = fsps->size();
  for (size_t idx = 0; idx < numofelements; idx++)
  {
    const size_t access_idx = ascending ? idx : numofelements - 1 - idx;
    const KeyIndexPair value = (*fsps)[access_idx];
    std::cout << access_idx << "\t" << value << std::endl;
  }
}

static FSPrioStore<KeyIndexPair> fill_fsp_store(const KeyIndexPair *present,
                                                size_t num_present,
                                                const KeyIndexPair *new_elem,
                                                CompareFunc<KeyIndexPair>
                                                    compare)
{
  FSPrioStore<KeyIndexPair> fsps(3);
  for (size_t idx = 0; idx < num_present; idx++)
  {
    fsps.add(&present[idx],compare);
  }
  fsps.add(new_elem,compare);
  return fsps;
}

static void unit_test_generic(bool ascending,
                              const std::vector<KeyIndexPair> &present,
                              const KeyIndexPair new_elem,
                              // FRAGE 6: Wozu dient is_this_new_elem?
                              // Es prüft, ob das hinzugefügte Element neu
                              // ist oder ob es sich um ein bereits
                              // vorhandenes Element handelt.
                              const std::vector<bool> &is_this_new_elem)
{
  const CompareFunc<KeyIndexPair> compare = ascending ? smaller : larger;
  const FSPrioStore<KeyIndexPair> fsps
  // FRAGE 7: Was liefert die Methode data(), angewandt auf present?
  // data() schreibt die Zeichen von present in ein Array. Es gibt
  // einen Pointer auf das Array zur"uck, der aus der Konvertierung von
  // present in das Array erhalten wurde.
    = fill_fsp_store(present.data(),present.size(),&new_elem,compare);
  assert(fsps.size() == 3);
  int next = 0;
  for (int idx = 0; idx < 3; idx++)
  {
    if (is_this_new_elem[idx])
    {
      assert(fsps[idx] == new_elem);
    } else
    {
      assert(fsps[idx] == present[next++]);
    }
  }
}

static void unit_test_1_asc(void)
{
  const std::vector<KeyIndexPair>present{{4.0,0},{7.0,1}};
  const KeyIndexPair new_elem(5.0,2);
  unit_test_generic(true,present,new_elem,{false,true,false});
}

static void unit_test_1_desc(void)
{
  const std::vector<KeyIndexPair>present{{7.0,0},{4.0,1}};
  const KeyIndexPair new_elem(5.0,2);
  unit_test_generic(false,present,new_elem,{false,true,false});
}

static void unit_test_2_asc(void)
{
  const std::vector<KeyIndexPair>present{{4.0,0},{7.0,1}};
  const KeyIndexPair new_elem(1.0,2);
  unit_test_generic(true,present,new_elem,{true,false,false});
}

static void unit_test_2_desc(void)
{
  const std::vector<KeyIndexPair>present{{7.0,0},{4.0,1}};
  const KeyIndexPair new_elem(1.0,2);
  unit_test_generic(false,present,new_elem,{false,false,true});
}

static void unit_test_3_asc(void)
{
  const std::vector<KeyIndexPair>present{{4.0,0},{7.0,1}};
  const KeyIndexPair new_elem(8.0,2);
  unit_test_generic(true,present,new_elem,{false,false,true});
}

static void unit_test_3_desc(void)
{
  const std::vector<KeyIndexPair>present{{7.0,0},{4.0,1}};
  const KeyIndexPair new_elem(8.0,2);
  unit_test_generic(false,present,new_elem,{true,false,false});
}

static void unit_test_4_asc(void)
{
  const std::vector<KeyIndexPair>present{{4.0,0},{7.0,1},{8.0,2}};
  const KeyIndexPair new_elem(9.0,2);
  unit_test_generic(true,present,new_elem,{false,false,false});
}

static void unit_test_4_desc(void)
{
  const std::vector<KeyIndexPair>present{{8.0,0},{7.0,1},{4.0,2}};
  const KeyIndexPair new_elem(9.0,2);
  unit_test_generic(false,present,new_elem,{true,false,false});
}

static void unit_test_5_asc(void)
{
  const std::vector<KeyIndexPair>present{{4.0,0},{7.0,1},{8.0,2}};
  const KeyIndexPair new_elem(5.0,2);
  unit_test_generic(true,present,new_elem,{false,true,false});
}

static void unit_test_5_desc(void)
{
  const std::vector<KeyIndexPair>present{{8.0,0},{7.0,1},{4.0,2}};
  const KeyIndexPair new_elem(5.0,2);
  unit_test_generic(false,present,new_elem,{false,false,true});
}

static void unit_test_6_asc(void)
{
  const std::vector<KeyIndexPair>present{{4.0,0},{7.0,1},{8.0,2}};
  const KeyIndexPair new_elem(3.0,3);
  unit_test_generic(true,present,new_elem,{true,false,false});
}

static void unit_test_6_desc(void)
{
  const std::vector<KeyIndexPair>present{{8.0,0},{7.0,1},{4.0,2}};
  const KeyIndexPair new_elem(3.0,3);
  unit_test_generic(false,present,new_elem,{false,false,false});
}

static void unit_tests(void)
{
  unit_test_1_asc();
  unit_test_1_desc();
  unit_test_2_asc();
  unit_test_2_desc();
  unit_test_3_asc();
  unit_test_3_desc();
  unit_test_4_asc();
  unit_test_4_desc();
  unit_test_5_asc();
  unit_test_5_desc();
  unit_test_6_asc();
  unit_test_6_desc();
}

typedef enum
{
  Undefined, /* must be first */
  OnlySmallest,
  OnlyLargest,
  SmallestLargest,
} Processmode;

static Processmode arg2process_mode(const char *tag)
{
  const char *tags[] = {"s","l","sl"};
  for (size_t idx = 0; idx < sizeof tags/sizeof tags[0]; idx++)
  {
    if (strcmp(tag,tags[idx]) == 0)
    {
      return (Processmode) (idx+1);
    }
  }
  return Undefined;
}

int main(int argc,char *argv[])
{
  int read_int;

  if (argc != 3 || arg2process_mode(argv[1]) == Undefined ||
      sscanf(argv[2],"%d",&read_int) != 1 || read_int < 1)
  {
    std::cerr << "Usage: " << argv[0] << " s|l|sl <capacity>" << std::endl;
    return EXIT_FAILURE;
  }
  unit_tests();
  const size_t capacity = (size_t) read_int;
  FSPrioStore<KeyIndexPair> *fsps_vec[3] = {nullptr};
  const Processmode process_mode = arg2process_mode(argv[1]);
  int indexes[3] = {-1,-1,-1};

  if (process_mode == OnlySmallest)
  {
    indexes[0] = 0;
  } else
  {
    if (process_mode == OnlyLargest)
    {
      indexes[0] = 1;
    } else
    {
      indexes[0] = 0;
      indexes[1] = 1;
    }
  }
  for (int j = 0; j < 3 && indexes[j] != -1; j++)
  {
    fsps_vec[indexes[j]] = new FSPrioStore<KeyIndexPair>(capacity);
  }
  double key;
  size_t input_items;
  for (input_items = 0; scanf("%lf\t%d",&key,&read_int) == 2; input_items++)
  {
    KeyIndexPair kip(key,static_cast<unsigned long>(read_int));
    for (int j = 0; j < 3 && indexes[j] != -1; j++)
    {
      const int i = indexes[j];
      fsps_vec[i]->add(&kip,i == 0 ? smaller : larger);
    }
  }
  std::cout << "# number of input items\t" << input_items << std::endl;
  for (int j = 0; j < 3 && indexes[j] != -1; j++)
  {
    const int i = indexes[j];
    output_extreme_values(fsps_vec[i],i == 0 ? "smallest" : "largest", i == 0);
    delete fsps_vec[i];
  }
  return EXIT_SUCCESS;
}
