#ifndef FS_PRIO_STORE_HPP
#define FS_PRIO_STORE_HPP
#include <cassert>
#include <cstdbool>
#include <cstdlib>

template<typename T>
using CompareFunc = bool (*)(const T *, const T *);
using KeyIndexPair = T
template <typename T>
class FSPrioStore
{
 private:
    KeyIndexPair *elements;
    unsigned long capacity, numofelements;
  // add private member variables

 public:
  FSPrioStore(size_t _capacity)
  {
    FSPrioStore *fsps = new FSPrioStore;/* one extra element for overflow */
    fsps->numofelements = 0;
    fsps->elements = new KeyIndexPair[1 + _capacity];
    fsps->capacity = _capacity;
  }
  // add initialization of private member variables
  

  ~FSPrioStore(void)
  {
    delete this->numofelements, this->elements, this->capacity;
  // implement destructor
  }


  bool is_full(void) const
  {
    assert(fsps != NULL);
    return (bool) (numofelements == capacity);
    }
  // implement this
  }

  size_t size(void) const
  {
  // implement this
    return this->numofelements;
  }

  void add(const T *value,CompareFunc<T> compare)
  {
  // implement this
  }

  T &operator[](size_t idx) const
  {
    assert(fsps != NULL && idx < size(void));
    return this->elements[idx];
  // implement this
  }
};
#endif
