#!/usr/bin/env python3

import sys, argparse, random
from timeit import Timer
from functools import partial

def runtime_get(func,*args):
  partial_object = partial(func,*args)
  times = Timer(partial_object).repeat(1,1)
  return min(times)

def parse_arguments(argv):
  default_width = 1000000
  default_lower_bound = -default_width/2
  default_upper_bound = default_width/2
  p = argparse.ArgumentParser(description='print pairs of floats and integers')
  p.add_argument('-l','--lower_bound',type=int,default=default_lower_bound,
                 help='specify lower bound, default: {}'
                       .format(default_lower_bound))
  p.add_argument('-u','--upper_bound',type=int,default=default_upper_bound,
                 help='specify upper bound, default: {}'
                       .format(default_upper_bound))
  p.add_argument('number',type=int,help='specify number of value to output')
  return p.parse_args(argv)

def generate_data(number,lower_bound,upper_bound):
  random.seed(a=34822384)
  for idx in range(number):
    r = random.uniform(lower_bound,upper_bound)
    print('{:.8f}\t{}'.format(r,idx))

def main():
  args = parse_arguments(sys.argv[1:])
  generate_data(args.number,args.lower_bound,args.upper_bound)

t = runtime_get(main)
sys.stderr.write('# time generating data: {:.2f} s\n'.format(t))
