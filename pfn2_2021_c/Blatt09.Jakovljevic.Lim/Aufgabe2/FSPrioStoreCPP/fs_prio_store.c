#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include "fs_prio_store.h"

struct FSPrioStore
{
  KeyIndexPair *elements;  /* the elements are stored here */
  size_t capacity,         /* the maximum number of elements stored */
         numofelements;    /* current number of elements stored */
};

FSPrioStore *fs_prio_store_new(size_t capacity)
{
  FSPrioStore *fsps = malloc(sizeof *fsps);

  assert(fsps != NULL && capacity > 0);
  /* one extra element for overflow */
  fsps->elements = malloc((capacity+1) * sizeof *fsps->elements);
  fsps->capacity = capacity;
  fsps->numofelements = 0;
  return fsps;
}

void fs_prio_store_delete(FSPrioStore *fsps)
{
  if (fsps != NULL)
  {
    free (fsps->elements);
    free (fsps);
  }
}

void fs_prio_store_reset(FSPrioStore *fsps)
{
  assert(fsps != NULL);
  fsps->numofelements = 0;
}

bool fs_prio_store_is_empty(const FSPrioStore *fsps)
{
  assert(fsps != NULL);
  return (bool) (fsps->numofelements == 0);
}

bool fs_prio_store_is_full(const FSPrioStore *fsps)
{
  assert(fsps != NULL);
  return (bool) (fsps->numofelements == fsps->capacity);
}

size_t fs_prio_store_size(const FSPrioStore *fsps)
{
  assert(fsps != NULL);
  return fsps->numofelements;
}

/* The idea of the method is to start at the back of the array
   and move an element to the next index, if it is larger than the
   key. In this way all elements larger than the key are shifted
   to the right, so that their order is preserved. At the end of the
   loop ptr points to the array location where we store the new element,
   if it is valid. The simplest way to implement this technique is to
   allocate an extra element in the array to be used for invalid elements
   (in the case the store is full).

   Technically we use a pointer ptr which initially points to the memory
   location after the last valid element. If numofelements is 0, then
   the for loop is not executed and ptr points to the first index
   in the array. As the store is not full, the new element is
   stored at the first index. If numofelements is > 0, then
   ptr > fsps->elements holds and the key is compared to
   the element pointed to by ptr-1. The loop iterates as long
   as ptr does not point to the first element of the array and the
   key of the new element is smaller then the element left of where
   ptr points to. Whenever both conditions hold, the new element must be
   inserted somewhere in the array and therefore all larger elements are
   shifted to the right by the copy operation *ptr = *(ptr-1).
   If ptr points to the first element of the array, the loop stops and
   if numofelements was > 0, the value stored in the first index was
   already moved to the right in the previous iteration.
   If ptr does not point to the first element of the array, but the
   key of the new element is larger or equal to (ptr - 1)->key,
   ptr points to a location in the index range from 0 to numofelements.
   If the store is not full or the location is not the overflow location,
   we store the new element. The number of elements in incremented by
   1 if the store was not full yet. The number of moves can be determined
   from the difference of the initial value of ptr at the start of the
   loop and the value at the end of the loop.
   The worst case of this method occurs if the elements are
   inserted in decreasing order. Then an element to be
   inserted is always smaller than the first element present. This means
   all element are moved to the right, thus requiring time on the order of
   k for each element kn steps for n elements. This
   worst case is unlikely to occur.
   In practice, however, the k minimum elements usually are not delivered
   in a specific order, so that the number of moves is usually very small,
   so that the use of a heap like data structure does not pay of, especially
   in cases where k is much smaller than n.
*/

size_t fs_prio_store_add_smallest(FSPrioStore *fsps,const KeyIndexPair *kvp)
{
  assert(fsps != NULL);
  KeyIndexPair *ptr;

  /* store elements such that the element of maximum
     priority is at the first index */
  for (ptr = fsps->elements + fsps->numofelements;
       ptr > fsps->elements && kvp->key < (ptr - 1)->key; ptr--)
  {
    *ptr = *(ptr - 1);
  }
  /* The following condition is not necessary, as in the case that

     fs_prio_store_is_full(fsps) && ptr >= fsps->elements + fsps->numofelements)

     holds, ptr points to the overflow location where we could store the
     new element without destroying anything.
  */
  if (!fs_prio_store_is_full(fsps) ||
      ptr < fsps->elements + fsps->numofelements)
  {
    *ptr = *kvp;
  }
  fsps->numofelements += (fs_prio_store_is_full(fsps) ? 0 : 1);
  return (size_t) ((fsps->elements + fsps->numofelements) - ptr);
}

size_t fs_prio_store_add_largest(FSPrioStore *fsps, const KeyIndexPair *kvp)
{
  assert(fsps != NULL);
  KeyIndexPair *ptr;

  /* store elements in reverse order, i.e.\ with the element of maximum
     priority at the first index */
  for (ptr = fsps->elements + fsps->numofelements;
       ptr > fsps->elements && kvp->key > (ptr - 1)->key; ptr--)
  {
    *ptr = *(ptr - 1);
  }
  if (!fs_prio_store_is_full(fsps) ||
      ptr < fsps->elements + fsps->numofelements)
  {
    *ptr = *kvp;
  }
  fsps->numofelements += (fs_prio_store_is_full(fsps) ? 0 : 1);
  return (size_t) ((fsps->elements + fsps->numofelements) - ptr);
}

KeyIndexPair fs_prio_store_at(const FSPrioStore *fsps, size_t idx)
{
  assert(fsps != NULL && idx < fs_prio_store_size(fsps));
  return fsps->elements[idx];
}
