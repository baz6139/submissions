#!/bin/bash

if test $# -ne 3
then
  echo "Usage: $0 s|l|sl <number of elements> <number of extremes to select>"
  exit 1
fi

mode=$1
number=$2
extremes=$3

INPUTFILE=`mktemp /tmp/kvp.XXXXXX` || exit 1
OUTPUTFILE=`mktemp /tmp/extremes.XXXXXX` || exit 1
smallest=0
largest=0
if test ${mode} = "s"
then
  smallest=1
else
  if test ${mode} = "l"
  then
    largest=1
  else
    if test ${mode} == "sl"
    then
      smallest=1
      largest=1
    fi
  fi
fi
./experiment.py ${number} > ${INPUTFILE}
touch ${OUTPUTFILE}
if test $smallest -eq 1
then
  sort -n ${INPUTFILE} | sort -n | head -n ${extremes} > ${OUTPUTFILE}
fi
if test $largest -eq 1
then
  sort -n ${INPUTFILE} | sort -n | tail -n ${extremes} >> ${OUTPUTFILE}
fi
TMPFILE1=`mktemp /tmp/kvp.XXXXXX` || exit 1
cat ${INPUTFILE} | ./fs_prio_store.x ${mode} ${extremes} > ${TMPFILE1}
if test $? -ne 0
then
  echo "$0: ./fs_prio_store.x ${mode} ${extremes} failed"
  exit 1
fi
TMPFILE2=`mktemp /tmp/kvp.XXXXXX` || exit 1
cat ${TMPFILE1} | grep -v '^#' | cut -f 2,3 > ${TMPFILE2}
diff ${TMPFILE2} ${OUTPUTFILE}
if test $? -ne 0
then
  echo "$0: ./fs_prio_store.x ${mode} ${extremes} delivers incorrect output"
  exit 1
fi
rm -f ${INPUTFILE} ${OUTPUTFILE} ${TMPFILE1} ${TMPFILE2}
