#include "simul_evolution.h"
#include <stdlib.h>
#include <stdio.h>

#define PRINT(...)                   \
    if (logfp != NULL)               \
    {                                \
        fprintf(logfp, __VA_ARGS__); \
    }                                \
    else                             \
    {                                \
        printf(__VA_ARGS__);         \
    }

size_t simulation_step(char *poparray, size_t popsize0, size_t population,
                       const double *divide_prob);

void simulation_run(const size_t *num_individuals, const double *divide_prob,
                    size_t generations, FILE *logfp)
{
    size_t i;
    size_t popsize[] = {num_individuals[0], num_individuals[1]};
    size_t population = num_individuals[0] + num_individuals[1];
    char *poparray;

    poparray = malloc(population * sizeof population);
    for (i = 0; i < population; i++)
    {
        poparray[i] = (i < num_individuals[0]) ? 0 : 1;
    }

    PRINT("step\tnum_0\tnum_1\n")

    for (i = 0; i < generations; i++)
    {
        popsize[0] = simulation_step(poparray, popsize[0], population,
                                     divide_prob);
        popsize[1] = population - popsize[0];

        PRINT("%lu\t%lu\t%lu\n", i, popsize[0], popsize[1])

        if (popsize[0] == 0 || popsize[1] == 0)
        {
            PRINT("fixed:%d\tsteps: %lu\n", (popsize[0] == 0) ? 1 : 0, i)
            free(poparray);
            return;
        }
    }

    PRINT("simulation stopped after %lu steps (0:%lu,1:%lu)\n", generations,
          popsize[0], popsize[0])
    free(poparray);
}

size_t simulation_step(char *poparray, size_t popsize0, size_t population,
                       const double *divide_prob)
{
    size_t i;
    int idx;
    for (i = 0; i < population; i++)
    {
        if (divide_prob[(int)poparray[i]] >= drand48())
        {
            idx = (int)(drand48() * population);
            if (poparray[i] != poparray[idx])
            {
                poparray[idx] = poparray[i];
                popsize0 += (poparray[i] == 0) ? 1 : -1;
            }
        }
    }
    return popsize0;
}

