#!/bin/sh

if test $# -ne 1
then
  echo "Usage: $0 <inputfile>"
  exit 1
fi
inputfile=$1
if test ! -f ${inputfile}
then
  echo "$0: file ${inputfile} does not exist"
  exit 1
fi
lines=`cat ${inputfile} | wc -l`
if test ${lines} -lt 2
then
  echo "$0: file ${inputfile} contains less than 2 lines"
  exit 1
fi
