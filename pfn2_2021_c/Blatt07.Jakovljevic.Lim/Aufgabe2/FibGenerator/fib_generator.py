#!/usr/bin/env python

import sys, argparse

def parse_arguments(argv):
  p = argparse.ArgumentParser(description='output fibonacci numbers')
  p.add_argument('--max_idx',type=int,default=93,
                 help=('specify maximum index of Fibonacci number to be '
                       'generated, default: 93'))
  return p.parse_args(argv)

args = parse_arguments(sys.argv[1:])

def fib_infinite():
  pp, p = 0, 1 # pp = previous of previous, p = previous
  while True:
    yield pp   # iteration will receive pp as value
    current_sum = p + pp
    pp = p
    p = current_sum

for idx, f in enumerate(fib_infinite()):
  print('{}\t{}'.format(idx,f))
  if idx == args.max_idx: # args delivered by option parser
    break
