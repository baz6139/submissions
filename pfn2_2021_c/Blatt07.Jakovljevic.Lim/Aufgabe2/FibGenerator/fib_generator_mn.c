#include <stdio.h>
#include <stdlib.h>
#include "fib_generator.h"

int main(void)
{
  FibGenerator *fib_generator = fib_generator_new();
  for (size_t idx = 0; idx <= 93; idx++)
  {
    size_t fib_num = fib_generator_next(fib_generator);
    printf("%lu\t%lu\n",idx,fib_num);
  }
  fib_generator_delete(fib_generator);
  return EXIT_SUCCESS;
}
