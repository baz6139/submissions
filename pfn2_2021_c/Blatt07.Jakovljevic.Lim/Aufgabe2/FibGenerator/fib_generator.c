#include <stdlib.h>
#include <assert.h>
#include "fib_generator.h"

struct FibGenerator {
    size_t p, pp;
};


FibGenerator *fib_generator_new(void) {
    FibGenerator *fibo = malloc(sizeof *fibo);
    
    assert(fibo != NULL);
    fibo->p = 0;
    fibo->pp = 1;

    return fibo;
}


void fib_generator_delete(FibGenerator *fib_generator) {
    if (fib_generator != NULL)
        free(fib_generator);
}


size_t fib_generator_next(FibGenerator *fib_generator) {
    assert (fib_generator != NULL);
    
    size_t first_term = fib_generator->p;

    fib_generator->p = fib_generator->pp;
    fib_generator->pp += first_term;
    return first_term;
}


/*Antwort zur Frage: Der Integer/Zahlen-Typ von Python
erm"oglicht beliebig große Zahl, indem die Zahl nicht auf
eine bestimmte Anzahl von Bits beschr"ankt, wie es C ist.
In C m"ussen wir einen Type f"ur unsere Variable
deklarieren, die Gr"osse der Ganzzahl unserer Variable begrenzt.
In diesem Fall liegt die Fibonacci-Zahlen nach der ersten 94.
ausserhalb der "Grenze" des von uns deklarierten Typs */

