#ifndef FIB_GENERATOR_H
#define FIB_GENERATOR_H
#include <stddef.h>

/* The name of the opaque datatyp which stores the state of the
   generation of Fibonacci numbers. In the file fib_generator.c
   one must add the declarion 
   struct FibGenerator
   {
     ...
   };
   with appropriate components between { and } 
*/

typedef struct FibGenerator FibGenerator;

/* dynamically create a space for a new FibGenerator structure,
   initialze it and return a pointer to it */

FibGenerator *fib_generator_new(void);

/* free the dynamically allocated space referred to by the 
    passed pointer */

void fib_generator_delete(FibGenerator *fib_generator);

/* deliver the next Fibonacci number. So the i-th call of the
   function delivers the i-th Fibonacci number */

size_t fib_generator_next(FibGenerator *fib_generator);

#endif
