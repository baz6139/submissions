#include <iostream>
#include <cstdbool>
#include <cstdlib>
#include <unistd.h>
#include <cassert>
#include <cstring>
#include <vector>
#include "option_parser.hpp"
#include "read_numbers.hpp"
#include "subsetsum.hpp"

bool *subsetsum(const size_t *arr,
                size_t r,
                size_t s)
{
    unsigned long i;
    bool *mark;
    bool result;
    mark = malloc(r * sizeof mark);
    for (i = 0; i < r; i++)
    {
        mark[i] = false;
    }

    result = subsetsum_calulate(arr, mark, r, s);

    if (result)
    {
        return mark;
    }
    else
    {
        free(mark);
        return nullptr;
    }
}

bool subsetsum_memo_calulate(DefinedValue **cache, const size_t *arr,
                             bool *mark, size_t r, size_t s)
{
    if (cache[r][s].defined)
    {
        return cache[r][s].has_sol;
    }
    if (s == 0)
    {
        return true;
    }
    if (r == 0)
    {
        return false;
    }
    bool result;
    if (s >= arr[r - 1])
    {
        result = subsetsum_memo_calulate(cache, arr, mark, \
                                         r - 1, s - arr[r - 1]);
        cache[r][s].defined = true;
        cache[r][s].has_sol = result;
        if (result)
        {
            mark[r - 1] = true;
            return true;
        }
    }
    mark[r - 1] = false;
    result = subsetsum_memo_calulate(cache, arr, mark, r - 1, s);
    cache[r][s].defined = true;
    cache[r][s].has_sol = result;
    return result;
}

bool *subsetsum_memo(const size_t *arr,
                     size_t r,
                     size_t s)
{
    unsigned long i;
    unsigned long j;

    bool *mark;
    bool result;
    mark = malloc(r * sizeof mark);
    for (i = 0; i < r; i++)
    {
        mark[i] = true;
    }
    DefinedValuesMatrix(size_t r + 1 ,size_t s + 1);
    
    DefinedValue **cache;
    result = subsetsum_memo_calulate(cache, arr, mark, r, s);

    for (i = 0; i <= r; i++)
    {
        for (j = 0; j <= s; j++)
        {
            cache[i][j].defined = false;
        }
    }

    array2dim_delete(cache);

    if (result)
    {
        return mark;
    }
    else
    {
        free(mark);
        return nullptr;
    }
}
