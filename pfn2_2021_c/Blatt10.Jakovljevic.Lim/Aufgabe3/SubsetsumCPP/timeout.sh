#!/bin/sh

# This shell script runs a program for up to 1 second
# and if the program has not completed after 1 second
# an error is reported.

if test $# -lt 2
then
  echo "Usage: $0 <max_seconds> <path to program> [option1] [option2] ..."
  exit 1
fi
seconds=$1
shift

$* &
pid=$!
sleep 1
kill $pid 2>/dev/null 
if test $? -eq 0
then
  echo "$0 killed the following command as it ran for more than ${seconds} seconds" 
  echo "$*"
  exit 1
fi
