#ifndef READ_NUMBERS_HPP
#define READ_NUMBERS_HPP
#include <iostream>
#include <stdexcept>
#include <vector>

std::vector<size_t> read_numbers(const char *filename)
{
  FILE *fp = fopen (filename,"r");

  if (fp == NULL)
  {
    throw std::ios_base::failure(std::string("Cannot open file ") + filename);
  }
  long number;
  std::vector<size_t> vector_of_numbers{};
  while (fscanf(fp,"%ld",&number) == 1)
  {
    if (number < 0)
    {
      throw std::ios_base::failure(std::string("file ") + filename + ", line " +
                                   std::to_string(vector_of_numbers.size()+1) +
                                   ": illegal negative number " +
                                   std::to_string(number));
    }
    vector_of_numbers.push_back(static_cast<size_t>(number));
  }
  fclose(fp);
  return vector_of_numbers;
}
#endif
