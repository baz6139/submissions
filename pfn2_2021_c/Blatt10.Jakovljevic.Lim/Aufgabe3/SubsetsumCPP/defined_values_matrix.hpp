#ifndef DEFINED_VALUES_MATRIX_HPP
#define DEFINED_VALUES_MATRIX_HPP
#include <cstdbool>
#include <cstdlib>

struct DefinedValues
{
  bool defined, // is the has_col entry already computed?
       has_solution; // does the entry correspond to values
                     // with a solution?
};

class DefinedValuesMatrix
{
  private:
    size_t rows, columns;
    DefinedValues ** M;
  public:
    DefinedValuesMatrix(size_t _rows,size_t _columns)
    {
      rows = _rows;
      columns = _columns;
      M = new DefinedValues * [_rows];
      M[0] = static_cast<DefinedValues *>(calloc(_rows * _columns,
                                                 sizeof *M[0]));
      for (size_t idx = 1; idx < rows; idx++)
      {
        M[idx] = M[idx-1] + _columns;
      }
    }
    ~DefinedValuesMatrix(void)
    {
      free(M[0]);
      delete [] M;
    }
    bool defined_is_set(size_t row, size_t col) const
    {
      assert (row < rows && col < columns);
      return M[row][col].defined;
    }
    bool has_solution_is_set(size_t row, size_t col) const
    {
      assert (row < rows && col < columns);
      return M[row][col].has_solution;
    }
    void has_solution_set(size_t row, size_t col, bool value)
    {
      assert (row < rows && col < columns);
      M[row][col].defined = true;
      M[row][col].has_solution = value;
    }
};
#endif
