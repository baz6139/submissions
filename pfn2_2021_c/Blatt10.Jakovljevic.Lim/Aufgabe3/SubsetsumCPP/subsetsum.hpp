#ifndef SUBSETSUM_HPP
#define SUBSETSUM_HPP
#include <cstddef>
#include <cstdbool>
/* Returns pointer mark != nullptr if the ordered set of numbers in
   <arr> of size <r> contains a
   subset with the sum of its members equal to <s>.
   In this case , <mark[i]> is true iff the <i>th
   element in <arr> is in the subset.
   If there is no subset of the elements in <arr>, then a nupptr-pointer is
   returned.
 */

bool *subsetsum(const size_t *arr,
                size_t r,
                size_t s);

/* The following function uses the memoization technique to compute the
   same result as the previous function, but with much less rekursive
   calls. */

bool *subsetsum_memo(const size_t *arr,
                     size_t r,
                     size_t s);

#endif
