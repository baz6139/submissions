#ifndef OPTION_PARSER_HPP
#define OPTION_PARSER_HPP
#include <iostream>
#include <cstdbool>
#include <cstdlib>
#include <unistd.h>
#include <cassert>
#include <stdexcept>

class Options
{
 private:
  bool use_memo_based_method, help_option;
  size_t lower_bound, upper_bound;
  const char *filename;

 public:
  Options(void)
      : use_memo_based_method(false),
        help_option(false),
        lower_bound(1),
        upper_bound(1000),
        filename(nullptr)
  {
  }
  void parse(int argc, char *const *argv)
  {
    int opt;
    while ((opt = getopt(argc, argv, "dfmlrh")) != -1)
    {
      switch ((char) opt)
      {
        case 'm':
          use_memo_based_method = true;
          break;
        case 'f':
          if (optind > argc - 1)
          {
            throw std::invalid_argument("missing arguments to option -f");
          }
          filename = argv[optind++];
          break;
        case 'r':
          if (optind + 1 > argc - 1)
          {
            throw std::invalid_argument("missing arguments to option -r");
          }
          long read_lower, read_upper;

          if (sscanf(argv[optind], "%ld", &read_lower) != 1 ||
              read_lower < 1 ||
              sscanf(argv[optind + 1], "%ld", &read_upper) != 1 ||
              read_lower > read_upper)
          {
            throw std::invalid_argument(
                        "two arguments to -r must be two integers, "
                        "with the first smaller or equal to the second");
          }
          lower_bound = (size_t) read_lower;
          upper_bound = (size_t) read_upper;
          optind += 2;
          break;
        case 'h':
          help_option = true;
          break;
        default:
          throw std::invalid_argument("-h shows the possible options");
          break;
      }
    }
    if (optind != argc)
    {
      throw std::invalid_argument("superfluous argument");
    }
    if (filename == nullptr)
    {
      throw std::invalid_argument("option -f is mandatory");
    }
  }
  bool use_memo_based_method_is_set(void) const
  {
    return use_memo_based_method;
  }
  bool help_option_is_set(void) const
  {
    return help_option;
  }
  size_t lower_bound_get(void) const
  {
    return lower_bound;
  }
  size_t upper_bound_get(void) const
  {
    return upper_bound;
  }
  const char *filename_get(void) const
  {
    return filename;
  }
  void usage(const char *progname)
  {
    const char *optionsmsg =
        "  -f <filename>  specify filename with elements of set S\n"
        "  -m             use memoization-based method\n"
        "  -r <int> <int> specify range of values for which to solve the\n"
        "                 subset sum problem (default: 1 1000)\n"
        "  -h             show this usage message";
    std::cerr << "Usage: " << progname << " [options]" << std::endl
              << "solve subset sum problem for numbers in given file."
              << std::endl
              << optionsmsg
              << std::endl;
  }
};
#endif
