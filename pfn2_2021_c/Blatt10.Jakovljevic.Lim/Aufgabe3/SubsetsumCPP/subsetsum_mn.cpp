#include <iostream>
#include <cstdbool>
#include <cstdlib>
#include <unistd.h>
#include <cassert>
#include <cstring>
#include <vector>
#include "option_parser.hpp"
#include "read_numbers.hpp"
#include "subsetsum.hpp"

static size_t show_subset(const size_t *arr,const bool *mark,size_t setsize)
{
  size_t sum = 0;
  bool first = true;

  for (size_t idx = 0; idx < setsize; idx++)
  {
     sum += mark[idx] ? arr[idx] : 0;
  }
  std::cout << sum << " = ";
  for (size_t idx = 0; idx < setsize; idx++)
  {
    if (mark[idx])
    {
      std::cout << (first ? "" : " + ") << arr[idx];
      first = false;
    }
  }
  std::cout << std::endl;
  return sum;
}

int main(int argc, char *argv[])
{
  Options options{};
  std::vector<size_t> vector_of_numbers;

  try
  {
    options.parse(argc, (char *const *) argv);
  }
  catch (std::invalid_argument const &ext)
  {
    std::cerr << argv[0] << ": " << ext.what() << std::endl;
    options.usage(argv[0]);
    return EXIT_FAILURE;
  }
  if (options.help_option_is_set())
  {
    options.usage(argv[0]);
    return EXIT_SUCCESS;
  }
  try
  {
    vector_of_numbers = read_numbers(options.filename_get());
  }
  catch (std::ios_base::failure const &ext)
  {
    std::cerr << argv[0] << ": " << ext.what() << std::endl;
    return EXIT_FAILURE;
  }
  size_t number;
  const size_t setsize = vector_of_numbers.size();
  size_t idx;
  const size_t *arr = vector_of_numbers.data();
  bool *mark;

  for (idx = 0; idx < setsize; idx++)
  {
    assert(idx == 0 || arr[idx-1] <= arr[idx]);
  }
  for (number = options.lower_bound_get();
       number <= options.upper_bound_get();
       number++)
  {
    if (options.use_memo_based_method_is_set())
    {
      mark = subsetsum_memo(arr,setsize,number);
    } else
    {
      mark = subsetsum(arr, setsize, number);
    }
    if (mark != nullptr)
    {
      const size_t this_sum = show_subset(arr,mark,setsize);
      assert(this_sum == number);
      free(mark);
    } else
    {
        std::cout << "no subset of sum " << number << " found" << std::endl;
    }
  }
  return EXIT_SUCCESS;
}
