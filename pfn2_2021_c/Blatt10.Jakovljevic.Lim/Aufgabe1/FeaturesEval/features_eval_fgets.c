#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include "sktimer.h"

int evaluate_single_prediction(int a,int b)
{
  return (a >= 130 && b >= 50) || b >= 70;
}

#include "features_eval.c"

int main(int argc, char *argv[])
{
  FILE *in_fp;
  const char *mode, *accum, *inputfile;
  const char *modes = "sscanf|atoi|ownloop";
  const char *accums = "twodim|casebycase|pllinc";
  bool haserr = false;
  GtSKtimer *timer;

  if (argc != 4)
  {
    fprintf(stderr,"Usage: %s %s %s <inputfile>\n",argv[0],modes,accums);
    return EXIT_FAILURE;
  }
  mode = argv[1];
  accum = argv[2];
  inputfile = argv[3];
  in_fp = fopen(inputfile,"r");
  if (in_fp == NULL)
  {
    fprintf(stderr,"%s: Cannot open \"%s\"\n",argv[0],inputfile);
    return EXIT_FAILURE;
  }
  timer = gt_SKtimer_new();
  gt_SKtimer_start(timer);
  if (evaluate_all(in_fp,mode,accum) != 0)
  {
    fprintf(stderr,"%s: first argument must match %s and ",argv[0],modes);
    fprintf(stderr,"second argument must match %s\n",accums);
    haserr = true;
  }
  fclose(in_fp);
  printf("# TIME fgets %s %s %.2fs\n",mode,accum,gt_SKtimer_elapsed(timer));
  return haserr ? EXIT_FAILURE : EXIT_SUCCESS;
}
