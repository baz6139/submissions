#!/usr/bin/env python3

import argparse, sys, random

def parse_arguments(argv):
  p = argparse.ArgumentParser(description='generate random feature table')
  p.add_argument('num_features',type=int,
                  help='specify number of feature lines to generate')
  return p.parse_args(argv)

args = parse_arguments(sys.argv[1:])

for _ in range(args.num_features):
  a = random.randint(0,100)
  b = random.randint(0,100)
  label = random.randint(0,1)
  print('{}\t{}\t{}'.format(a,b,label))
