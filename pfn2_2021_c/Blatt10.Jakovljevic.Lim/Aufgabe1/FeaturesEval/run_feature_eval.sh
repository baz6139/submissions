#!/bin/sh

if test $# -ne 1
then
  echo "Usage: $0 <inputfile with features>"
  exit 1
fi

feature_file=$1
./features_eval_line_store.x ${feature_file}
for mode in sscanf atoi ownloop
do 
  for accum in twodim casebycase pllinc
  do
    ./features_eval_fgets.x $mode $accum small_feature_table.tsv | diff -I '^#' - small_output.txt
    ./features_eval_fgets.x $mode $accum ${feature_file}
  done
done
./features_eval_line_store.x small_feature_table.tsv | diff -I '^#' - small_output.txt
./features_eval_line_store.x ${feature_file}
