./features_eval_qli.x small_feature_table.tsv | diff -I '^#' - small_output.txt
./features_eval_qli.x /Users/stefan/features.tsv
tn=11691851,fn=518,fp=10234631,tp=247681,recall=99.79%,precision=2.36%
# TIME qli own_loop case_by_case 0.61s
tn=11691851,fn=518,fp=10234631,tp=247681,recall=99.79%,precision=2.36%
# TIME fgets sscanf 2dim 5.07s
tn=11691851,fn=518,fp=10234631,tp=247681,recall=99.79%,precision=2.36%
# TIME fgets sscanf case_by_case 5.06s
tn=11691851,fn=518,fp=10234631,tp=247681,recall=99.79%,precision=2.36%
# TIME fgets sscanf parallel_inc 5.10s
tn=11691851,fn=518,fp=10234631,tp=247681,recall=99.79%,precision=2.36%
# TIME fgets atoi 2dim 2.90s
tn=11691851,fn=518,fp=10234631,tp=247681,recall=99.79%,precision=2.36%
# TIME fgets atoi case_by_case 2.91s
tn=11691851,fn=518,fp=10234631,tp=247681,recall=99.79%,precision=2.36%
# TIME fgets atoi parallel_inc 2.95s
tn=11691851,fn=518,fp=10234631,tp=247681,recall=99.79%,precision=2.36%
# TIME fgets own_loop 2dim 1.47s
tn=11691851,fn=518,fp=10234631,tp=247681,recall=99.79%,precision=2.36%
# TIME fgets own_loop case_by_case 1.49s
tn=11691851,fn=518,fp=10234631,tp=247681,recall=99.79%,precision=2.36%
# TIME fgets own_loop parallel_inc 1.51s
