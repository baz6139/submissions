#!/usr/bin/env python3

import sys

def output_the_function(read_mode,accum):
  print('static void evaluate_features_{}_{}(FILE *in_fp)'
         .format(read_mode,accum))
  print('''{
  const size_t line_buf_size = 512;
  char *line = malloc(line_buf_size * sizeof *line);
  ''')
  if accum == 'twodim':
    print('  size_t tn, fn, fp, tp;')
    print('  size_t count[2][2] = {{0,0},{0,0}};')
  else:
    print('  size_t tn = 0, fn = 0, fp = 0, tp = 0;')
  print('''
  while (fgets(line,line_buf_size-1,in_fp) != NULL)
  {
    int a, b, label, prediction;
  ''',end='')
  if read_mode == 'sscanf':
    if accum == 'twodim':
      print("/*lst{{FeaturesEvalReadmode{}}}*/".format(read_mode),end='')
    print('''
    if (sscanf(line,"%d %d %d",&a,&b,&label) != 3)
      break;
    ''',end='')
    if accum == 'twodim':
      print('/*lstend*/')
  elif read_mode == 'atoi':
    if accum == 'twodim':
      print("/*lst{{FeaturesEvalReadmode{}}}*/".format(read_mode),end='')
    print('''
    char *secondsep, *firstsep = strchr(line,'\\t');
    a = atoi(line);
    secondsep = strchr(firstsep+1,'\\t');
    b = atoi(firstsep + 1);
    label = atoi(secondsep + 1);
    ''',end='')
    if accum == 'twodim':
      print('/*lstend*/')
  elif read_mode == 'ownloop':
    if accum == 'twodim':
      print("/*lst{{FeaturesEvalReadmode{}}}*/".format(read_mode),end='')
    print('''
    const char *sptr;
    for (a = 0, sptr = line; *sptr != '\\t'; sptr++)
      a = a * 10 + (int) (*sptr - '0');
    for (b = 0, sptr++; *sptr != '\\t'; sptr++)
      b = b * 10 + (int) (*sptr - '0');
    for (label = 0, sptr++; *sptr != '\\n'; sptr++)
      label = label * 10 + (int) (*sptr - '0');
    ''',end='')
    if accum == 'twodim':
      print('/*lstend*/')
  else:
    sys.stderr.write('read_mode {} is not defined'.format(read_mode))
    exit(1)
  print('prediction = evaluate_single_prediction(a,b);')
  if accum == 'twodim':
    if read_mode == 'ownloop':
      print("/*lst{{FeaturesEvalAccum{}}}*/".format(accum))
    print('    count[prediction][label]++;')
    if read_mode == 'ownloop':
      print('/*lstend*/')
  elif accum == 'casebycase':
    if read_mode == 'ownloop':
      print("/*lst{{FeaturesEvalAccum{}}}*/".format(accum),end='')
    print('''
    if (prediction == 0)
    {
      if (label == 0) tn++; else fn++;
    } else
    {
      if (label == 0) fp++; else tp++;
    }
    ''')
    if read_mode == 'ownloop':
      print('/*lstend*/')
  elif accum == 'pllinc':
    if read_mode == 'ownloop':
      print("/*lst{{FeaturesEvalAccum{}}}*/".format(accum),end='')
    print('''
    /* pllinc: paralell increment */
    tn += (prediction == 0 && label == 0); 
    fn += (prediction == 0 && label == 1);
    fp += (prediction == 1 && label == 0);
    tp += (prediction == 1 && label == 1);
    ''')
    if read_mode == 'ownloop':
      print('/*lstend*/')
  print('  }')
  if accum == 'twodim':
    print('''
  tn = count[0][0];
  fn = count[0][1];
  fp = count[1][0];
  tp = count[1][1];''',end='')
  print('''
  free(line);
  printf("tn=%lu,fn=%lu,fp=%lu,tp=%lu,",tn,fn,fp,tp);
  printf("recall=%.2f%%,",100.0 * (double) tp/(tp + fn));
  printf("precision=%.2f%%\\n",100.0 * (double) tp/(tp + fp));
}
  ''')

modes = ['sscanf','atoi','ownloop']
accums = ['twodim','casebycase','pllinc']

print('/* This file is generated by {}. Do not edit.*/'
       .format(' '.join(sys.argv)))

for read_mode in modes:
  for accum in accums:
    output_the_function(read_mode,accum)

print('static int evaluate_all(FILE *in_fp,const char *mode,const char *accum)')
print('{')
for read_mode in modes:
  for accum in accums:
    print('  if (strcmp(mode,"{}") == 0 && strcmp(accum,"{}") == 0)'
           .format(read_mode,accum))
    print('  {')
    print('    evaluate_features_{}_{}(in_fp);'.format(read_mode,accum))
    print('    return 0;')
    print('  }')
print('''
  return -1;
}
''')
