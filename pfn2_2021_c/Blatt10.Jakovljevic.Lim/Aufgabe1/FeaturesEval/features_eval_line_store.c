#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include "sktimer.h"
#include "pfn_file_info.h"
#include "pfn_line_store.h"

int evaluate_single_prediction(int a,int b)
{
  return (a >= 130 && b >= 50) || b >= 70;
}

static void evaluate_features_line_store_case_by_case(const PfNLineStore 
                                                       *line_store)
{
  size_t tn = 0, fn = 0, fp = 0, tp = 0,
         idx, num_lines = pfn_line_store_number(line_store);

  for (idx = 0; idx < num_lines; idx++)
  {
    int a, b, label, prediction;
    const char *sptr, *current_line = pfn_line_store_access(line_store,idx);

    for (a = 0, sptr = current_line; *sptr != '\t'; sptr++)
    {
      a = a * 10 + (int) (*sptr - '0');
    }
    for (b = 0, sptr++; *sptr != '\t'; sptr++)
    {
      b = b * 10 + (int) (*sptr - '0');
    }
    for (label = 0, sptr++; *sptr != '\0'; sptr++)
    {
      label = label * 10 + (int) (*sptr - '0');
    }
    prediction = evaluate_single_prediction(a,b);
    if (prediction == 0)
    {
      if (label == 0)
      {
        tn++;
      } else
      {
        fn++;
      }
    } else
    {
      if (label == 0)
      {
        fp++;
      } else
      {
        tp++;
      }
    }
  }
  printf("tn=%lu,fn=%lu,fp=%lu,tp=%lu,",tn,fn,fp,tp);
  printf("recall=%.2f%%,",100.0 * (double) tp/(tp + fn));
  printf("precision=%.2f%%\n",100.0 * (double) tp/(tp + fp));
}
  
int main(int argc, char *argv[])
{
  const char *inputfile;
  bool haserr = false;
  GtSKtimer *timer;
  size_t file_size;
  PfNFileInfo *file_info;

  if (argc != 2)
  {
    fprintf(stderr,"Usage: %s <inputfile>\n",argv[0]);
    return EXIT_FAILURE;
  }
  inputfile = argv[1];
  timer = gt_SKtimer_new();
  gt_SKtimer_start(timer);
  file_info = pfn_file_info_new(argv[0],inputfile);
  if (file_info == NULL)
  {
    haserr = true;
  }
  if (!haserr)
  {
    file_size = pfn_file_info_size(file_info);
    if (file_size > 0)
    {
      void *file_contents = pfn_file_info_contents(file_info);
      PfNLineStore *line_store 
        = pfn_line_store_new((unsigned char *) file_contents,file_size,'\n');
      evaluate_features_line_store_case_by_case(line_store);
      pfn_line_store_delete(line_store);
    }
    printf("# TIME qli own_loop case_by_case %.2fs\n",
             gt_SKtimer_elapsed(timer));
  }
  gt_SKtimer_delete(timer);
  return haserr ? EXIT_FAILURE : EXIT_SUCCESS;
}
