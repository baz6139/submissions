#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
  FILE *in_fp;
  int cc, num = 0, values[3], colcount = 0;
  const char *inputfile;

  if (argc != 2)
  {
    fprintf(stderr,"Usage: %s <inputfile>\n",argv[0]);
    return EXIT_FAILURE;
  }
  inputfile = argv[1];
  in_fp = fopen(inputfile,"r");
  if (in_fp == NULL)
  {
    fprintf(stderr,"%s: Cannot open \"%s\"\n",argv[0],inputfile);
    return EXIT_FAILURE;
  }
  while ((cc = fgetc(in_fp)) != EOF)
  {
    if (cc >= (int) '0' && cc <= (int) '9')
    {
      num = (num * 10) + (cc - '0');
    } else
    {
      values[colcount] = num;
      colcount = (colcount + 1) % 3;
      num = 0;
    }
  }
  fclose(in_fp);
  return EXIT_SUCCESS;
}
