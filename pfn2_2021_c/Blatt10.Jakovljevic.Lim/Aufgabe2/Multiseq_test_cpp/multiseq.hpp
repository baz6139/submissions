#ifndef MULTISEQ_H
#define MULTISEQ_H
#include <cstddef>
#include <vector>

/* implement a class to store multiple sequence stored in a
   file using the simplified multiple fasta format */

class Multiseq
{
  private:
  std::vector<const unsigned char *> header_vector;
  std::vector<const unsigned char *> sequence_vector;
  const unsigned char *sequence_end;
  size_t _maximum_sequence_length;
  size_t _total_sequence_length;
  public:
  Multiseq(const char *filename,unsigned char *filecontents,
           size_t totallength);
  ~Multiseq(void);
  size_t num_of_sequences(void) const;
  size_t maximum_sequence_length(void) const;
  size_t total_sequence_length(void) const;
  size_t sequence_length(size_t seqnum) const;
  const unsigned char *sequence_ptr(size_t seqnum) const;
  size_t header_length(size_t seqnum) const;
  const unsigned char *header_ptr(size_t seqnum) const;
  void show(size_t) const;
  size_t * char_dist_get(void) const;
};

#endif
