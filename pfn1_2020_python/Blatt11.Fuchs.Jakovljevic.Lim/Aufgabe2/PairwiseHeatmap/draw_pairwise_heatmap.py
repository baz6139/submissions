#!/usr/bin/env python3

import sys, re, argparse
import numpy as np
from matplotlib import pyplot as plt
plt.switch_backend('agg')

class PairwiseScores:
  def __init__(self,inputfile):
    self._title = None
    self._row_key = list()
    self._column_key = list()
    self._score = dict()
    try:
      stream = open(inputfile)
    except IOError as err:
      sys.stderr.write('{}: {}\n'.format(sys.argv[0],err))
      exit(1)
    for line in stream:
      comment = re.search(r'^# *(.*)',line)
      if comment:
        if (self._title is None):
          self._title = comment.group(1)
      else:                   # kein Kommentar
        [row, column, score] = line.rstrip().split()
        score = float(score)
        if row not in self._row_key:
          self._row_key.append(row)
        if column not in self._column_key:
          self._column_key.append(column)
        self._score[row,column] =  score
    self._row_key.sort()
    self._column_key.sort()
    m = len(self._row_key)
    n = len(self._column_key)
    self._pwsm = np.empty((m,n))
    self._pwsm.fill(np.NaN)
    for i, row in enumerate(self._row_key):
      for j, column in enumerate(self._column_key):
        if (row,column) in self._score:
          self._pwsm[i,j] = self._score[row,column]
  '''
  Den Scores die auf np.NaN gesetzt sind werden in der durch matshow 
  erzeugten Heatmap die Farbe weiß zugewiesen, auch wenn diese Farbe 
  in der Colormap nicht enthalten ist.
  '''
  def lookup(self,rk,ck):
    if (rk,ck) in self._score:
      return self._score[rk,ck]
    else:
      return None
  def row_keys(self):
    return self._row_key
  def column_keys(self):
    return self._column_key
  def title(self):
    return self._title
  def np_matrix(self):
    return self._pwsm

def parse_arguments():
  p = argparse.ArgumentParser(description=('plot heatmap of scores from '
                                           'pairwise comparisons'))
  p.add_argument('--fontsize_labels',type=int,default=3,metavar='<int>',
                 help='specify fontsize of labels, default: 3')
  p.add_argument('--fontsize_title',type=int,default=10,metavar='<int>',
                 help='specify fontsize of title, default: 10')
  p.add_argument('inputfile',type=str,help=('specify input file with at least '
                                            'three columns, where the first '
                                            'two columns contain keys for '
                                            'and the third column is a numeric '
                                            'value'))
  return p.parse_args()

def main():
  args = parse_arguments()
  pairwise_scores = PairwiseScores(args.inputfile)
  pairwise_score_matrix = pairwise_scores.np_matrix()
  this_cmap = 'afmhot'
  fig, ax = plt.subplots()
  im = ax.matshow(pairwise_score_matrix,cmap=this_cmap)
  # create the bar with color and their association with scores
  fig.colorbar(im, ax = ax)
  ax.set_xticks(list(range(0,len(pairwise_scores.column_keys()))))
  ax.set_yticks(list(range(0,len(pairwise_scores.row_keys()))))
  ax.set_xticklabels(pairwise_scores.column_keys(),
                     fontsize=args.fontsize_labels,rotation='vertical')
  ax.set_yticklabels(pairwise_scores.row_keys(),
                     fontsize=args.fontsize_labels,rotation='horizontal')
  if pairwise_scores.title():
    ax_title = pairwise_scores.title()
  else:
    ax_title = ('heatmap of data in file {}'.format(args.inputfile))
  ax.set_title(ax_title,fontsize=args.fontsize_title)
  outfile = '{}_heat_{}.pdf'.format(args.inputfile,this_cmap)
  fig.savefig(outfile)

if __name__ == '__main__':
  main()


