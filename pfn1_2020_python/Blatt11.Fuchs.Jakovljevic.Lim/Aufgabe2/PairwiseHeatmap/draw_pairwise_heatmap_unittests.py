#!/usr/bin/env python3

import unittest
import numpy as np
from draw_pairwise_heatmap import PairwiseScores

class TestPairwiseScores(unittest.TestCase):
  def test_matrix(self):
    inputfile = 'alignment_scores.tsv'
    triples = None
    with open(inputfile) as stream:
      triples = [line.rstrip().split('\t') for line in stream if line[0] != '#']
    expected_row_keys = len(set([v[0] for v in triples]))
    expected_column_keys = len(set([v[1] for v in triples]))
    pairwise_scores = PairwiseScores(inputfile)
    if pairwise_scores.title() is not None:
      expected_title = 'scores of the local alignments of 100 protein sequences'
      self.assertEqual(pairwise_scores.title(),expected_title)
    row_keys =  pairwise_scores.row_keys()
    column_keys =  pairwise_scores.column_keys()
    self.assertEqual(len(row_keys),expected_row_keys)
    self.assertEqual(len(column_keys),expected_column_keys)
    pairwise_score_matrix = pairwise_scores.np_matrix()
    count_defined = 0
    sum_scores = 0
    for i in range(len(row_keys)):
      rk = row_keys[i]
      for j in range(len(column_keys)):
        if not np.isnan(pairwise_score_matrix[i,j]):
          ck = column_keys[j]
          self.assertEqual(pairwise_score_matrix[i,j],
                           pairwise_scores.lookup(rk,ck))
          count_defined += 1
          sum_scores += pairwise_score_matrix[i,j]
    self.assertEqual(count_defined,len(triples))
    self.assertEqual(sum_scores,sum([float(v[2]) for v in triples]))

unittest.main()
