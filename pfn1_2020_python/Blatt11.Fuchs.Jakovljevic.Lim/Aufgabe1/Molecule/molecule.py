import sys

class Atom:
    def __init__(self,ident,name,x,y,z,atomtype,*optional_list):
      self._ident = ident
      self._name = name
      self._x = x
      self._y = y
      self._z = z
      self._atomtype = atomtype
      self._optional_list = optional_list
      return
    def __str__(self):
      text = '{} {} {} {} {} {}'.\
               format(self._ident,self._name,self._x,self._y,self._z,\
               self._atomtype)
      for optional_list_el in self._optional_list:
        text += ' ' + optional_list_el
      return text
    def __sub__(self,other):  # required only for LJ-Potential
      # TODO
      return

class Bond:
    def __init__(self,ident,atomid1,atomid2,kind,*optional_list):
      self._ident = ident
      self._atomid1 = atomid1
      self._atomid2 = atomid2
      self._kind = kind
      self._optional_list = optional_list
      return
    def atomid1(self):
      return self._atomid1
    def atomid2(self):
      return self._atomid2
    def __str__(self):
      text = '{} {} {} {}'.\
        format(self._ident,self._atomid1,self._atomid2,self._kind)
      for optional_list_el in self._optional_list:
        text += ' ' + optional_list_el
      return text

class Molecule:
    def __init__(self,name,atom_list,bond_list):
      self._name = name
      self._atom_list = [Atom(*atom_ls) for atom_ls in atom_list]
      self._bond_list = [Bond(*bond_ls) for bond_ls in bond_list]
      return
    def name(self):
      return self._name
    def atoms_number(self):
      return len(self._atom_list)
    def atom_list(self):
      return self._atom_list
    def bond_list(self):
      return self._bond_list
    def bonds_number(self):
      return len(self._bond_list)
    def __str__(self):
      head_molecule = '@<TRIPOS>MOLECULE'
      head_atom = '@<TRIPOS>ATOM'
      head_bond = '@<TRIPOS>BOND'
      text = head_molecule + '\n' + self._name + '\n' + head_atom + '\n'
      for atom in self._atom_list:
        text += '{}\n'.format(atom)
      text += head_bond + '\n'
      for bond in self._bond_list:
        text += '{}\n'.format(bond)
      text = text.rstrip('\n')
      return text
    def __sub__(self,other):  # required only for LJ-Potential
      # TODO
      return
    def adjacency_matrix(self): # needed for shortest path algorithm
      # TODO
      return
