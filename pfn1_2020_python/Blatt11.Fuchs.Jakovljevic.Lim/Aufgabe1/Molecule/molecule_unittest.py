#!/usr/bin/env python3

import unittest, re
from mol2iter import mol2Iterator
from molecule import Molecule

class TestMolecule(unittest.TestCase):
  def test_str_atom_bond(self):
    filenames = ['water.mol2','water_methanole.mol2','protein.mol2',
                 'five_molecules.mol2']
    for inputfile in filenames:
      lines = None
      first_atom = list()
      first_bond = list()
      with open(inputfile) as stream:
        lines = stream.readlines()
        for idx, line in enumerate(lines):
          if re.search(r'^@<TRIPOS>ATOM',line):
            first_atom.append(idx + 1)
          elif re.search(r'^@<TRIPOS>BOND',line):
            first_bond.append(idx + 1)
      mol_number = 0
      for molecule_name, atom_list, bond_list in mol2Iterator(inputfile):
        molecule = Molecule(molecule_name,atom_list,bond_list)
        for idx, atom in enumerate(molecule.atom_list()):
          olist = lines[first_atom[mol_number]+idx].split()
          slist = str(atom).split()
          self.assertEqual(olist,slist)
        for idx, bond in enumerate(molecule.bond_list()):
          olist = lines[first_bond[mol_number]+idx].split()
          slist = str(bond).split()
          self.assertEqual(olist,slist)
        mol_number += 1

unittest.main()
