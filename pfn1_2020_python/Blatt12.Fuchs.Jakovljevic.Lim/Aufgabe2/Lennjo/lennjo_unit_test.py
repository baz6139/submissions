#!/usr/bin/env python3

import unittest, string
from molecule import Molecule
from mol2iter import mol2Iterator

def enumerate_molecule_pairs(filename):
  molecule_list = list()
  molecule_names = set()
  for name, atom_line_list, bond_line_list in mol2Iterator(filename):
    if name in molecule_names:
      sys.stderr.write('{}: molecule name {} already occurred'
                       .format(sys.argv[0],name))
      exit(1)
    molecule_list.append(Molecule(name,atom_line_list,bond_line_list))
    num_molecules = len(molecule_list)
    for i in range(0,num_molecules-1):
      m_i = molecule_list[i]
      for j in range(i+1,num_molecules):
        m_j = molecule_list[j]
        yield m_i, m_j

class TestPwGenFunctions(unittest.TestCase):
  def test_lennjo(self):
    lennjo_values = {('argon1','argon2') :	-0.230915,
                     ('pentane','neopentane') :	-6.930721,
                     ('benzene','pyridine') :	-4.439783,
                     ('water','methanole') :	33.243382}

    successful_tests = set()
    for filename in ['argon_dimer','benzene_pyridine_pipi',
                     'pentane_neopentane','water_methanol']:
      for m1, m2 in enumerate_molecule_pairs('{}.mol2'.format(filename)):
        key = (m1.name(),m2.name())
        if key in lennjo_values:
          ljp = m1 - m2
          self.assertTrue(abs(ljp - lennjo_values[key]) <= 1e-4)
          successful_tests.add(key)
    self.assertEqual(successful_tests,set(lennjo_values.keys()))

unittest.main()
