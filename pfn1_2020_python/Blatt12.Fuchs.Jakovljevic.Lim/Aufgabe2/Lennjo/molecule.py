import sys, re
from math import sqrt

def atom_value_get(table,elem):
  elem = re.sub(r'[0-9]$','',elem)
  if not elem in table:
    sys.stderr.write('{}: no value for {} in {}\n'
                     .format(sys.argv[0],elem,table.__name__))
    exit(1)
  return table[elem]

class Atom:
    def __init__(self,ident,name,x,y,z,atomtype,optional_list):
        self._ident = ident
        self._name = name
        self._x = float(x)
        self._y = float(y)
        self._z = float(z)
        self._atomtype = atomtype
        self._optional_list = optional_list
    def name(self):
      return self._name
    def __str__(self):
      outlist = [self._ident,self._name]
      for f in [self._x,self._y,self._z]:
        outlist.append('{:.4f}'.format(f))
      outlist.append(self._atomtype)
      if self._optional_list:
        outlist += self._optional_list
      return ' '.join(outlist)
    def __sub__(self,other):  # required only for LJ-Potential
      d2 = (self._x - other._x)**2 + (self._y - other._y)**2 \
             + (self._z - other._z)**2
      d = sqrt(d2)
      return d

class Bond:
    def __init__(self,ident,atomid1,atomid2,kind,optional_list):
      self._ident = ident
      self._atomid1 = atomid1
      self._atomid2 = atomid2
      self._kind = kind
      self._optional_list = optional_list
    def atomid1(self):
      return self._atomid1
    def atomid2(self):
      return self._atomid2
    def __str__(self):
        outlist = [self._ident,self._atomid1,self._atomid2,self._kind]
        if self._optional_list:
          outlist += self._optional_list
        return ' '.join(outlist)

class LennartJonesData():
  '''
  (Einheiten: epsilon in kcal/mol, sigma in AA)
  Daten stammen aus OPLS-AA-Kraftfeld, vereinfachte Annahme: nur 1 Satz
  Parameter pro Atomsorte
  '''
  lj_epsilon = { 'H' : 0.1260, 'He': 0.0200, 'C' : 0.1050, 'N' : 0.1700,
                 'O' : 0.2100, 'Ne': 0.0690, 'Cl': 0.3000, 'Ar': 0.2339}
  lj_sigma = { 'H' : 2.420, 'He': 2.556, 'C' : 3.750, 'N' : 3.200,
               'O' : 2.960, 'Ne': 2.780, 'Cl': 3.400, 'Ar': 3.401}
  '''
  the following two methods are class methods, i.e. they access data
  represented only once per class  and independen of any instance of the
  class. The access is via the arguments
  LennartJonesData.lj_epsilon and
  LennartJonesData.lj_sigma. The parameter elem is the name of
  the element. For an instance a of class Atom this name is delivered by
  a.name(). So call the methods like this:
        epsilon_a = LennartJonesData.epsilon_get(a.name())
        sigma_a = LennartJonesData.sigma_get(a.name())
  '''
  def epsilon_get(elem):
    return atom_value_get(LennartJonesData.lj_epsilon,elem)
  def sigma_get(elem):
    return atom_value_get(LennartJonesData.lj_sigma,elem)


class Molecule:
    def __init__(self,name,atom_line_list,bond_line_list):
      self._name =name
      self._atom_list = list()
      self._bond_list = list()
      for a in atom_line_list:
        self._atom_list.append(Atom(a[0],a[1],a[2],a[3],a[4],a[5],a[6:]))
      for b in bond_line_list:
        self._bond_list.append(Bond(b[0],b[1],b[2],b[3],b[4:]))
    def name(self):
      return self._name
    def atoms_number(self):
      return len(self._atom_list)
    def atom_list(self):
      return self._atom_list
    def bond_list(self):
      return self._bond_list
    def bonds_number(self):
      return len(self._bond_list)
    def __str__(self):
      lines = list()
      lines.append('@<TRIPOS>MOLECULE')
      lines.append(self._name)
      lines.append('@<TRIPOS>ATOM')
      for a in self._atom_list:
        lines.append(str(a))
      lines.append('@<TRIPOS>BOND')
      for b in self._bond_list:
        lines.append(str(b))
      return '\n'.join(lines)
    def __sub__(self,other):  # required only for LJ-Potential
      lenjo = 0
      for atom_s in self.atom_list():
        for atom_o in other.atom_list():
          if atom_s.name() == atom_o.name():
            epsilon = LennartJonesData.epsilon_get(atom_s.name())
            sigma = LennartJonesData.sigma_get(atom_s.name())
          else:
            epsilon_s = LennartJonesData.epsilon_get(atom_s.name())
            epsilon_o = LennartJonesData.epsilon_get(atom_o.name())
            epsilon = sqrt(epsilon_s*epsilon_o)
            sigma_s = LennartJonesData.sigma_get(atom_s.name())
            sigma_o = LennartJonesData.sigma_get(atom_o.name())
            sigma = (sigma_s + sigma_o)/2
          lenjo += 4 * epsilon*((sigma/(atom_s - atom_o))**12 - \
                      (sigma/(atom_s-atom_o))**6)
      return lenjo
    def adjacency_matrix(self): # needed for shortest path algorithm
      # TODO
      return
