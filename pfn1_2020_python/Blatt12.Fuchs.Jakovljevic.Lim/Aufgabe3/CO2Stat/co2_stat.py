#!/usr/bin/env python3
from bs4 import BeautifulSoup
import re
from data_matrix_class import DataMatrix
from float_or_None import float_or_None
import matplotlib.pyplot as plt
plt.switch_backend('agg')
from collections import OrderedDict
from country2region import Country2Region

def convert_table_headers(h_list):
  century = '19'
  for i, h_el in enumerate(h_list):
    if re.search(r'^\d{4}$',h_el):
      m = re.search(r'(\d{2})\d{2}',h_el)
      century = m.group(1)
    elif re.search(r"^'\d{2}$",h_el):
      h_list[i] = re.sub(r"'",century,h_el)
  return h_list

def co2_stat_table_lines_get(co2_stat_html):
  soup = BeautifulSoup(co2_stat_html, features = 'html.parser')
  table = soup.find('table', attrs = {'class': 'table-responsive'})
  assert table
  thead = table.find('thead')
  tbody = table.find('tbody')
  h_list = [th.text for th in thead.find_all('th')]
  h_list = convert_table_headers(h_list)
  h_list.insert(1, 'is_country')
  co2_stat_table_lines = ['\t'.join(h_list)]
  for tr_line in tbody.find_all('tr'):
    data_fields = list()
    for i, td in enumerate(tr_line.find_all('td')):
      if i == 0:
        is_state = 0 if td.find('b') else 1
        state_field = td.text.strip()
        m = re.search(r'\d*\s+(.+)', state_field)
        state = m.group(1)
        data_fields.append(state)
        data_fields.append(str(is_state))
      else:
        value_field = td.text.strip()
        co2_value = re.sub(r',','.',value_field)
        data_fields.append(co2_value)
    co2_stat_table_lines.append('\t'.join(data_fields))
  return co2_stat_table_lines


class CO2Stat:
  def __init__(self, co2_stat_table_lines):
    self._dm = DataMatrix(co2_stat_table_lines, key_col = 0, ordered = True)
    a_list = self._dm.attribute_list()
    self._year_list = [attr for attr in a_list 
                            if re.search(r'\d{4}', attr)]
    self._all_countries = [k for k in self._dm.keys() 
                             if self._dm[k]['is_country']=='1']

  def show_orig(self):
    self._dm.show_orig('\t',self._dm.attribute_list(),self._dm.keys())
  
  def plot_for_country_list(self,country_list,color_list,groupname,prefix):
    fig,ax = plt.subplots()
    years = [int(y) for y in self._year_list]
    for i, country in enumerate(country_list):
      emissions = [float_or_None(self._dm[country][y])    
                                   for y in self._year_list]
      # self._dm[country][y]  ruft Methode __getitem__ in Datamatrix auf 
      ax.plot(years, emissions, color=color_list[i], label=country)
    ax.legend(loc='best',fontsize='small')
    ax.set_xlabel('years')
    ax.set_ylabel('$CO_2$ emission per person (tons)')
    ax.set_title(('$CO_2$ emissions for some {} countries'.format(groupname)))
    figname = '{}.pdf'.format(prefix)
    fig.savefig(figname)
  
  def histogram_all_emissions(self,prefix):
    emissions = [float(self._dm[c][y]) for c in self._dm.keys() 
                                       for y in self._year_list 
                                       if self._dm[c][y]]
    # -> damit erhaelt man die musterloesung
    # wenn man nur Länder möchte: 
    #emissions = [float(self._dm[c][y]) for c in self._all_countries 
    #                                   for y in self._year_list 
    #                                   if self._dm[c][y]]
    fig,ax = plt.subplots()
    ax.set_xlabel("emissions per person for all countries (tons)")
    ax.set_ylabel("number of events")
    ax.set_title("distribution of $CO_2$ emission values")
    ax.hist(emissions, bins=750)
    ax.set_xlim(0,20)
    fig.savefig("histogram.pdf")
  
  def groupby(self,select_func):
    groups = OrderedDict()
    for country in self._all_countries:
      g = select_func(country)
      if not g:     # 'None' ausschliessen
        continue
      if not g in groups:     
        groups[g] = list()
      for y in self._year_list:
        if self._dm[country][y]:
          groups[g].append(float(self._dm[country][y]))
    return groups
  
  def boxplot_by_unit(self,for_continent,prefix):
    cr = Country2Region()
    unit = 'continent' if for_continent else 'region'
    fig, ax = plt.subplots()
    ax.set_title('distribution of $CO_2$ emission values by {}'.format(unit))
    ax.set_ylabel('$CO_2$ emission per person (tons)')
    if for_continent:
      groups = self.groupby(cr.continent)
    else:
      groups = self.groupby(cr.region)
    ax.boxplot(groups.values(),labels=groups.keys())
    ax.set_xticklabels(groups.keys(),rotation=25,ha='center',size='x-small')
    # ha='center' entspricht der Musterlösung, ha='right' der Aufgabenstellung
    fig.savefig('boxplot_{}.pdf'.format(unit))

