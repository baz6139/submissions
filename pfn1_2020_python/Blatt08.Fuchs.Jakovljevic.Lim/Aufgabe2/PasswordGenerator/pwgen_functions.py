#!/usr/bin/env python3

import re, random

# Unittests:
# Die Methode test_structure_elements prueft zunächst mit 
# assertTrue(structure_string_is_valid(struct_str)), dass korrekte 
# Strukturstrings (Beispiele definiert in Zeile 11) in 
# structure_string_is_valid als korrekt erkannt werden.
# self.assertEqual(list(structure_elements_enumerate(struct_str)),
# struct_str_pairs) prüft dann, dass structure_elements_enumerate die 
# Strukturstrings korrekt in Paare umgewandelt. 
# In self.assertFalse(structure_string_is_valid(struct_str)) wird geprüft, 
# dass für falsche Strukturstrings (Zeile 23)
# von structure_string_is_valid False ausgegeben wird. 

# Die Methode test_randstring prüft für 2 Beispielalphabete und 5 Werte für 
# n jeweils 10 mal, dasss die Länge der von randstring(alphabet,n) erzeugten
# zufälligen Strings jeweils korrekt ist, und das alle in den Strings  
# vorkommenden Buchstaben im jeweiligen Alphabet enthalten sind.

# Die Methode test_word_list prüft, dass die Listen der Worte im von 
# word_dict_get erzeugten Dictonary jeweils die korrekte Länge haben.

def structure_string_is_valid(struct_str):
  m = re.search(r'^([dpw]{1}\d+)+$', struct_str)
  if m:
    return True

def structure_elements_enumerate(struct_str):
  matches = re.findall(r'[dpw]{1}\d+', struct_str)
  for struct_element in matches:
    s = struct_element[0]
    n = int(struct_element[1:])
    yield (s, n)

def randstring(alphabet,n):
  rand_string = ''
  alpha_size = len(alphabet)
  for i in range(n):
    rand_string += alphabet[random.randint(0,alpha_size-1)]
  return rand_string

def word_dict_get():
  wd = dict()
  stream = open('wordlist.txt','r')
  for line in stream:
    word = line.rstrip('\n')
    if len(word) not in wd:
      wd[len(word)] = []
    wd[len(word)].append(word)
  stream.close()
  return wd
