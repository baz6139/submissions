#!/usr/bin/env python3

import re, sys, os, argparse, string
import numpy as np
from random import randint
from pwgen_functions import structure_string_is_valid, \
                            structure_elements_enumerate, \
                            randstring, \
                            word_dict_get

def password_generate(wd, struct_str):
  if not structure_string_is_valid(struct_str):
    sys.stderr.write('Incorrect structure string {}.\n'.format(wd))
    exit(1)
  pw = ''
  choice_list = []
  for struct in structure_elements_enumerate(struct_str):
    struct_number = struct[1]
    struct_letter = struct[0]
    if struct_letter == 'd':
      pw += randstring(string.digits,struct_number)
      choice_list.append(len(string.digits)**struct_number)
    elif struct_letter == 'p':
      pw += randstring(string.punctuation,struct_number)
      choice_list.append(len(string.punctuation)**struct_number)
    elif struct_letter == 'w':
      if not struct_number in wd:
        sys.stderr.write(
             'There are no words of length {} in wordlist.txt.\n'.\
             format(struct_number))
        exit(1)
      word_list = wd[struct_number]
      pw += word_list[randint(0,len(word_list)-1)]
      choice_list.append(len(word_list))
  return (pw,choice_list)


def parse_command_line():
  p = argparse.ArgumentParser(description=('extract information '
                                             'of password generator'))
  p.add_argument('-s', '--structure', type=str, default='w4p2d2w5p1d1w8',
                 help='specify structure string, default is w4p2d2w5p1d1w8')
  p.add_argument('-n', '--number', type=int, default=1, 
                 help='specify number of passwords, default is 1')
  return p.parse_args()


def main():
  try:
    args = parse_command_line()
  except argparse.ArgumentTypeError as err:
    sys.stderr.write('{}: {}\n'.format(sys.argv[0],err))
    exit(1)
  word_dict = word_dict_get()
  pw, choice_list = password_generate(word_dict,args.structure)
  print('number of possible passwords of structure {}: {:.2e}'
         .format(args.structure,np.prod(choice_list)))
  for _ in range(args.number):
    pw, _ = password_generate(word_dict,args.structure)
    print('{}'.format(pw))

if __name__ == '__main__':
  main()

