#!/usr/bin/env python3

def month_dict_get_comprehension():
  month_list = ['Jan','Feb','Mar','Apr','May','Jun',\
                'Jul','Aug','Sep','Oct','Nov','Dec']
  return {m: idx for idx, m in enumerate(month_list)}

def partial_sum_gen_comprehension(g):
  return (x for x in [0] for i in g for x in [x + i])



#### TEST (da 'make test' mit list und nicht mit generator testet ######
'''
def lgen():
  l = [3,2,3,4,2,9]
  for i in l:
    yield i

g = lgen()
print(list(partial_sum_gen_comprehension(g)))
'''
