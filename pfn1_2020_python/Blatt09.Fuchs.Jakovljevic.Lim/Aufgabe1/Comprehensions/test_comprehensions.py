#!/usr/bin/env python3

import unittest
from comprehensions import month_dict_get_comprehension, \
                           partial_sum_gen_comprehension

def month_dict_get():
  month_list = ['Jan','Feb','Mar','Apr','May','Jun',\
                'Jul','Aug','Sep','Oct','Nov','Dec']
  month_dict = dict()
  for idx, m in enumerate(month_list):
    month_dict[m] = idx
  return month_dict

def partial_sum_gen(g):
  psum = 0
  for a in g:
    psum += a
    yield psum

class TestComprehensions(unittest.TestCase):
  def test_month_dict(self):
    month_list = ['Jan','Feb','Mar','Apr','May','Jun',\
                  'Jul','Aug','Sep','Oct','Nov','Dec']
    self.assertEqual(month_dict_get(),month_dict_get_comprehension())
  def test_partial_sum(self):
    l = [3,2,3,4,2,9]
    self.assertEqual(list(partial_sum_gen(l)),
                     list(partial_sum_gen_comprehension(l)))

unittest.main()
