#!/usr/bin/env python3
from math import sqrt
import sys 

def listofvectors_read(filename):
  stream = open(filename,'r')
  vectors = [] 
  for line in stream:
    vectors.append(eval(line))
  stream.close()
  return vectors  

def rmsd_evaluate(v_vectors, w_vectors):
  if len(v_vectors) !=len(w_vectors): 
    sys.stderr.write('Length of vectors {} and {} have to be equal\n'.\
                     format(w_vectors,w_vectors))
    exit(1)

  for vectors in [v_vectors, w_vectors]:
    for points in vectors:
      if len(points) != 3:
        sys.stderr.write('The points in vectors {} and {} must be triplets\n'.\
                         format(v_vectors,w_vectors))
        exit(1) 

  a = 0
  for i in range(len(v_vectors)):
    for j in range(3):
      a += (v_vectors[i][j] - w_vectors[i][j])**2
  rmsd = sqrt(a/len(v_vectors))
  return rmsd

def listofvectors_rmsd_print(listofvectors):
  for i in range(0,len(listofvectors),2):
    rmsd = rmsd_evaluate(listofvectors[i],listofvectors[i+1])
    print('{}\t{}\t{:.5f}'.format(i+1, i+2, rmsd))

vectors = listofvectors_read(sys.argv[1])
listofvectors_rmsd_print(vectors)
