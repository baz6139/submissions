#!/usr/bin/env python3

import sys
import re

if len(sys.argv)!=3:
  sys.stderr.write('Usage: ./gen_email.py <file_gen_email> <file_data>\n')
  exit(1)
file_data=sys.argv[2]
file_gen_email=sys.argv[1]
try:
  file_data_stream= open(file_data, 'r')
  file_gen_stream= open(file_gen_email, 'r')
except IOError as err:
  sys.stderr.write('Incorrect filename\n')
  exit(1)

###### lab data  #########

lab_dict = dict()
for line in file_data_stream:
   m = re.search(r'(_[A-Z]+_)\t(.+)', line)
   lab_dict[m.group(1)] = m.group(2)

file_data_stream.close()

######  MAIL  ##########
mail = file_gen_stream.read()

file_gen_stream.close()

mail_elements = re.findall(r'\w+|\W+', mail)

for el in mail_elements:
   if el in lab_dict:
     print(lab_dict[el], end='')
   else:
     print(el, end='')

