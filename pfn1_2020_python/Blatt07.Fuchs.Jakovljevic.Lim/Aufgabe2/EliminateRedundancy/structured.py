#!/usr/bin/env python3
import math
x_vector = [6,7,12,14,23,41,53,60,69,72,100,90]
y_vector = [2.5,1.1,6.3,2.1,2.9,15.3,20.7,18.4,22,33,50,43]
z_vector = [1,12,18,33,78,99,65,77,81,54,78,77]

def compare(list1, list2):
  diff1_2_diff_prod_sum, diff1_sqr_sum, diff2_sqr_sum = 0, 0, 0,
  for x, y in zip(list1, list2):
    diff1, diff2 = x - sum(list1)/len(list1), y - sum(list2)/len(list2)
    diff1_2_diff_prod_sum += diff1 * diff2
    diff1_sqr_sum += diff2 ** 2
    diff2_sqr_sum += diff1 ** 2
  return diff1_2_diff_prod_sum / math.sqrt(diff1_sqr_sum * diff2_sqr_sum)
print('x_vector/y_vector: {:.5f}'.format(compare(x_vector, y_vector)))
print('x_vector/z_vector: {:.5f}'.format(compare(x_vector, z_vector)))
print('y_vector/z_vector: {:.5f}'.format(compare(y_vector, z_vector)))
