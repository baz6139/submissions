#!/usr/bin/env python3


# Mathematische Ausdruck für Länge von s_i:
# 2^i - 1

def skyline(alphabet, i):
  sk_list = []
  sk_list.append(alphabet[0])
  for i in range(1,i):
    sk_i = sk_list[i-1] + alphabet[i] + sk_list[i-1]
    sk_list.append(sk_i)
  return sk_list


alphabet = list("abcdefghijklmnopqrstuvwxyz")

skylinelist = skyline(alphabet,9)
for sk in skylinelist:
  print(sk)

