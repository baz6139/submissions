#!/usr/bin/env python3

import re, sys

def usage():
  sys.stderr.write('Usage: {} <linewidth> <inputfile>\n'
                   .format(sys.argv[0]))
  exit(1)

if len(sys.argv) != 3:
  usage()

filename = sys.argv[2]

try:
  linewidth = int(sys.argv[1])
except ValueError as err:
  sys.stderr.write('{}: cannot convert {} into integer\n'.\
                         format(filename, sys.argv[1]))
### 
# korrigiert:
#  ursprünglich
# sys.stderr.write('{}: cannot convert {} into integer\n'.
#                             format(filename, err))
# und filename = sys.argv[2] wurde benutzt bevor es definiert wurde.
### 
  usage()

try:
  stream = open(filename, 'r')
except IOError as err:
  sys.stderr.write('{}: {}\n'.format(filename, err))
  exit(1)

# add your code here
for line in stream:
  line = line.rstrip()
  matches = re.findall(r'(\S+)', line)

  if not matches:
    # newlinematch = re.search(r'(\n)', line)
    print(line)

  i = 0
  while i < len(matches):
    wordlist = []
    lettercount = 0
    while lettercount < linewidth and i < len(matches):
      m = matches[i]
      lettercount += len(m)
      if lettercount > linewidth:
        break
      wordlist.append(m)
      i += 1
      if lettercount == linewidth:
        break
      lettercount += 1   # Leerzeichen
       # Zeilen dürfen nicht mit Leerzeichen enden, deswegen erst hier addiert.
    print(' '.join(wordlist))

#
stream.close()
