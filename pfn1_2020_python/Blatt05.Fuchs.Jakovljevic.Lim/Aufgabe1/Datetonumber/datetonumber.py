#!/usr/bin/env python3

import sys
import re

if len(sys.argv) != 2:
  sys.stderr.write('Usage: {} <inputfile with dates in format dd.mm.yyyy>\n'.\
                        format(sys.argv[0]))
  exit(1)

fname = sys.argv[1]
try:
  stream = open(fname,'r')
except IOError as err:
  sys.stderr.write('{}: {}\n'.format(sys.argv[0],err))
  exit(1)

month_dict = {
  1: 0, 2: 31, 3: 59, 4: 90, 5: 120, 6: 151, 
  7: 181, 8: 212, 9: 243, 10: 273, 11: 304, 12: 334
  }

for line in stream:
  m = re.search(r'(\d{2}).(\d{2}).(\d{4})', line)
  if m:
    day = int(m.group(1))
    month = int(m.group(2))
    year = int(m.group(3))
  
  if month > 2 and ((year % 4 == 0 and year % 100 != 0) or (year % 400 == 0)):
    s = 1
  else:
    s = 0

  dm = month_dict[month] + s
 
  summe = day + dm

  print('{}\t{}'.format(line.strip(), summe))
