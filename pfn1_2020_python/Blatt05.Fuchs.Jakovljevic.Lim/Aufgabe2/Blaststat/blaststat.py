#!/usr/bin/env python3

import sys, re
from math import log10


if len(sys.argv) != 2:
  sys.stderr.write('Usage: {} <inputfile>\n'.format(sys.argv[0]))
  exit(1)

inputfile = sys.argv[1]
try:
  stream = open(inputfile)
except IOError as err:
  sys.stderr.write('{}: {}\n'.format(sys.argv[0],err))
  exit(1)


################## define dictionaries  #########################

dist_score = dict()
dist_ew = dict()
dist_id = dict()

##################################################################

score_string = 'Score\s*=\s*(\d+)\s*bits\s*\(\d+\)\s*,\s*'
ew_string = 'Expect\s*=\s*(0.0|\d+e[+-]?\d+)'
score_ew_string = score_string + ew_string
id_string = 'Identities\s*=\s*\d+/\d+\s*\((\d{1,3})%\)'

for line in stream:
  score_ew_match = re.search(r'{}'.format(score_ew_string), line)

  #### Erwartungswert und Bitscore #####
  if score_ew_match:
    score = int(score_ew_match.group(1))
    ew = float(score_ew_match.group(2))
    if ew != 0:
      log_ew = round(log10(ew))
    else:
      log_ew = 0
    print("V\t{}\t{}".format(score,log_ew))

    ###### add to dist ######
    if not (score in dist_score):
      dist_score[score] = 0
    if not (log_ew in dist_ew):
      dist_ew[log_ew] = 0

    dist_score[score] += 1
    dist_ew[log_ew] += 1
  else:
     #### Prozent-Identitäts-Werte #####
    id_match = re.search(r'{}'.format(id_string), line)
    if id_match:
      identity = int(id_match.group(1))
      print("V\t{}".format(identity))
       ###### add to dist ######
      if not (identity in dist_id):
        dist_id[identity] = 0
      dist_id[identity] += 1



##############  print distributions   ###############
if dist_score:
  print("# distribution of bitscores\n" +
           "# Fields: bitscore, occurrences")
  for k in sorted(dist_score):
    print('D\t{}\t{}'.format(k,dist_score[k]))


if dist_id:
  print("# distribution of identities\n" +
           "# Fields: identity, occurrences")
  for k in sorted(dist_id):
    print('D\t{}\t{}'.format(k,dist_id[k]))


if dist_ew:
  print("# distribution of log10(expect)\n" + 
           "# Fields: log10(expect), occurrences")
  for k in sorted(dist_ew):
    print('D\t{}\t{}'.format(k,dist_ew[k]))

