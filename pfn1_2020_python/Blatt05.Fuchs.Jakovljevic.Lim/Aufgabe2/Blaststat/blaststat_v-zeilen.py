#!/usr/bin/env python3

import sys, re
from math import log10


if len(sys.argv) != 2:
  sys.stderr.write('Usage: {} <inputfile>\n'.format(sys.argv[0]))
  exit(1)

inputfile = sys.argv[1]
try:
  stream = open(inputfile)
except IOError as err:
  sys.stderr.write('{}: {}\n'.format(sys.argv[0],err))
  exit(1)


##################  Values  ################################
score_string = 'Score\s*=\s*(\d+)\s*bits\s*\(\d+\)\s*,\s*'
ew_string = 'Expect\s*=\s*(\d+.\d+|\d+e[+-]?\d*)'
score_ew_string = score_string + ew_string
id_string = 'Identities\s*=\s*\d+/\d+\s*\((\d{1,3})%\)'

for line in stream:
  line = line.rstrip()

  score_ew_match = re.search(r'{}'.format(score_ew_string), line)

  if score_ew_match:
    score = int(score_ew_match.group(1))
    ew = float(score_ew_match.group(2))
    if ew != 0:
      log_ew = round(log10(ew))
    else:
      log_ew = 0
    print("V\t{}\t{}".format(score,log_ew))
  else:
    id_match = re.search(r'{}'.format(id_string), line)
    if id_match:
      rel_anteil = int(id_match.group(1))
      print("V\t{}".format(rel_anteil))
