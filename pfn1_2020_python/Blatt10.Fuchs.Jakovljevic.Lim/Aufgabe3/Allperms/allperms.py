#!/usr/bin/env python3

from math import factorial
import sys, argparse

def all_permutations(elems):
  all_perms = []
  all_perms_rec(all_perms,elems)
  return sorted(all_perms)


def all_perms_rec(p,e):
  if len(e) == 0:
    p.append([])
  if len(e) == 1:
    p.append(e.copy())
  elif len(e) > 1:
    for idx in range(len(e)):
      e_reduced = e.copy()
      el_removed = e_reduced.pop(idx)
      all_perms_rec(p, e_reduced)
      p.sort(key=len)
      i = 0
      min_len = len(p[0])
      while i < len(p) and len(p[i]) == min_len:
        p[i].append(el_removed)
        i += 1


def all_permutations_verify(all_perms,elems):
  assert len(all_perms) == factorial(len(elems))
  # set: enthält keine Duplikate
  assert len(set(tuple(p) for p in all_perms)) == len(all_perms)
  for perm in all_perms:
    assert sorted(perm) == elems 
  return



def parse_command_line(argv):
  p = argparse.ArgumentParser(description='generate and verify permutations')
  p.add_argument('setsize',type=int, help='specify size of set to permute')
  return p.parse_args(argv)

def main():
  args = parse_command_line(sys.argv[1:])
  elems = list(range(args.setsize))
  all_perms = all_permutations(elems)
  all_permutations_verify(all_perms,elems)

if __name__ == '__main__':
  main()

