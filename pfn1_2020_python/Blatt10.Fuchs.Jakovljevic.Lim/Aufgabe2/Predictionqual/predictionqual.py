#!/usr/bin/env python3

import argparse
import re

def parse_command_line():
  p = argparse.ArgumentParser(description = 
                            'Compare quality of method with gold standard')
  p.add_argument('-g', '--gold_standard', type=str, 
               default='goldstandard.tsv',help='specify gold standard')
  p.add_argument('inputfile', nargs='+', type=str,
                  help='specify inputfile (mandatory argument)')
  return p.parse_args()

def inputfile2set(inputfile):
  pairsets = set()
  for line in inputfile:
    m = re.search(r'(\d+)\t(\d+)', line)
    pairsets.add((int(m.group(1)), int(m.group(2)) ))
  return pairsets

def evaluate(gs,prediction):
  true_positives_set = gs & prediction
  tp = len(true_positives_set)
  se = 100 * tp/len(gs)
  sp = 100 * tp/len(prediction)
  return tp, se, sp

def harmonic_mean(a,b):
  return 2/(1/a + 1/b)

def main():
  try:
    args = parse_command_line()
  except argparse.ArgumentTypeError as err:
    sys.stderr.write('{}: {}\n'.format(sys.argv[0],err))
    exit(1)
  try:
    stream_gold = open(args.gold_standard)
    gold_input = stream_gold.readlines()
  except IOError as err:
    sys.stderr.write('{}: {}\n'.format(sys.argv[0],err))
    exit(1)
  print('#filename\ttp\tsens\tspec\thmean')
  for file in args.inputfile:
    try:
      stream_prediction = open(file)
      prediction_input = stream_prediction.readlines()
    except IOError as err:
      sys.stderr.write('{}: {}\n'.format(sys.argv[0],err))
      exit(1)
    coords_prediction = inputfile2set(prediction_input)
    coords_gold = inputfile2set(gold_input)
    tp, sens, spec = evaluate(coords_gold, coords_prediction)
    hmean = harmonic_mean(sens, spec)
    text = '{:s}\t{}\t{:<.2f}\t{:<.2f}\t{:.2f}'.\
                  format(file, tp, sens, spec, hmean)
    print(text)

if __name__ == '__main__':
  main()
