#!/usr/bin/env python3

import argparse
import re

gold = 'goldstandard.tsv'
meth = 'prediction01.tsv'




stream_gold = open(gold)
gold_input = stream_gold.readlines()

stream_prediction = open(meth)
prediction_input = stream_prediction.readlines()

def inputfile2set(inputfile):
  pairsets = set()
  for line in inputfile:
    m = re.search(r'(\d+)\t(\d+)', line)
    pairsets.add((int(m.group(1)), int(m.group(2)) ))
  return pairsets


def evaluate(gs,prediction):
  true_positives_set = gs & prediction
  se = true_positives/len(gs)
  sp = true_positives/len(prediction)
  return true_positives, se, sp

coords_prediction = inputfile2set(prediction_input)
coords_gold = inputfile2set(gold_input)
tp, sens, spec = evaluate(coords_gold, coords_prediction)


main()

