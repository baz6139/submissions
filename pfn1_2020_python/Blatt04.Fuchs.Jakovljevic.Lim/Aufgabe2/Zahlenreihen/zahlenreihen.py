#!/usr/bin/env python3

import sys

################### Eingabe Überprüfungen   ##############
if len(sys.argv) != 2:
  sys.stderr.write('Usage: {} <k>\n'.format(sys.argv[0]))
  exit(1)


try:
  k = int(sys.argv[1])
except ValueError as err:
  formatstring = '{}: cannot convert "{}" to int\n'
  sys.stderr.write(formatstring.format(sys.argv[0],sys.argv[1]))
  exit(1)

if k <= 0:
  formatstring = "{}: parameter {} is not positive int\n"
  sys.stderr.write(formatstring.format(sys.argv[0],sys.argv[1]))
  exit(1)


###################  eigentliche Berechnungen  ############
print("Reihe a")

sum = 0
for i in range(2,2*k+1,2):
  print(i)
  sum += i

print("Summe: {}".format(sum))




print("Reihe b")

divisor = 2
sum = 0
for i in range(1,k+1):
  y = (3 + 2*i) / divisor
  print("{:.5e}".format(y))
  sum += y
  divisor = divisor * 2

print("Summe: {:.5e}".format(sum))





print("Reihe c")

sum = 0
y = 1
for i in range(1,k+1):
  print("{:.5e}".format(y))
  sum += y
  y = y * (-1/2)
  
print("Summe: {:.5e}".format(sum))





print("Reihe d")

y = 1
sum = 0
for i in range(1,k+1):
  y = y/i
  print("{:.5e}".format(y))
  sum += y

print("Summe: {:.5e}".format(sum))
