#!/usr/bin/env python3

import sys
import re


#################### Fehler-Behandlung ###############
if len(sys.argv) != 2:
  formatstring = 'Usage: {} <integer>\n'
  sys.stderr.write(formatstring.format(sys.argv[0]))
  exit(1)


arg = sys.argv[1]

x = arg.strip()


zahl = re.search(r'([+-]?\d+)',x)
if zahl.group(1) != x:
  formatstring_int = '{}: argument "{}" is not an integer\n'
  sys.stderr.write(formatstring_int.format(sys.argv[0],arg))
  exit(1)


#################### Quersumme berechnen ###############
x_abs = re.sub(r'[+-]','',x)


search_term = '(\d)'
ziffern = re.findall(r'{}'.format(search_term),x_abs)

quersumme = 0
for ziffer in ziffern:
  quersumme += ord(ziffer) - 48

print('{}\t{}'.format(x,quersumme))


