#!/usr/bin/env python3

n = 10

# [1] convert to while loop:
print("==== 1 =====")
i = 1
while i < n:
  print(i)
  i += 3


# [2] convert to for loop:
print("==== 2 =====")
for k in range(0, n):
  if (k >= n-k):
    break
  print(k, n-k)


# [3] convert to while loop:
print("==== 3 =====")
k = n
while k > -n:
  print(k)
  k -= 1


# [4] convert to for loop:
print("==== 4 =====")
for k in range(1,n+1):
  print(k)

