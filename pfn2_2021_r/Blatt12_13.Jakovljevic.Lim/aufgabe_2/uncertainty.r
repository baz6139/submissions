

true_prob <- c(0.25, 0.14, 0.24, 0.11)

true_prob <- c(true_prob, 1 - sum(true_prob))

parties <- c('green', 'spd', 'csu', 'afd', 'others')


onesamp <- function(n, true_prob){

    s <- sample(1:5, n, replace=TRUE, true_prob)

    l <- c(length(s[s == 1]),length(s[s == 2]), length(s[s == 3]), length(s[s == 4]), length(s[s == 5]))

    return (l / n)

}

calculate <- function(n_sampling, n_people){


    ms <- replicate(n_sampling, onesamp(n_people, true_prob))

    rownames(ms) <- parties

    cat("Data collected from", n_people,  "people and with", n_sampling, "resampling\n")
    cat("party expected median (95% limits)")

    for (party in parties) {
        cat(party,
            mean(ms[party,]),
            median(ms[party,]),
            quantile(ms[party,], c(0.025)),
            '-',
            quantile(ms[party,],c(0.975)),
            "\n")
    }

    cat("\n")
}

for (i in c(500, 1000, 2000)) {
    calculate(20000, i)
}

