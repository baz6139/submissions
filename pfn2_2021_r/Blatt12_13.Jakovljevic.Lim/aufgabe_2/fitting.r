
data <- read.table("default.dat" ,sep=" ", header=TRUE)
#default values used

sinexp <- function (x, phase, freq, decay) {
    return (sin(x * freq + phase) * exp(-1 * decay * x))
}

nlmod <- nls(y~sinexp(x, phase, freq, decay), data=data, start=list(phase=3, freq=150, decay=30))

png(file='jocelyne_fit.png')

plot (data$x, data$y, xlab = expression (italic(x)), ylab="amplitude")
lines(data$x, predict(nlmod), col = 3, lwd = 3)

dev.off()

